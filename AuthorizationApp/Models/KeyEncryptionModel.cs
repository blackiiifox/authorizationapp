﻿using AuthorizationApp.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthorizationApp.Models
{

    public class KeyMessageInfoModel : INotifyPropertyChanged
    {
        /// <summary>
        /// Id обладателя ключа
        /// </summary>
        public int IdAccount { get; set; }

        /// <summary>
        /// Открытый ключ для шифрования сообщений
        /// </summary>
        [JsonProperty(PropertyName = "OpenKey")]
        public long OpenKey { get; set; }

        /// <summary>
        /// Время создания ключа
        /// </summary>
        [JsonProperty(PropertyName = "TimeCreate")]
        public DateTime TimeCreate { get; set; }

        public long n { get; set; }

        public KeyMessageInfoModel()
        {
        }

        public KeyMessageInfoModel(AccountModel account, KeyMessageEncryptionModel key)
        {
            IdAccount = account.ID;
            OpenKey = key.OpenKey;
            TimeCreate = key.TimeCreate;
            n = key.n;
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }

    public class KeyMessageEncryptionModel : INotifyPropertyChanged, ICloneable
    {
        /// <summary>
        /// Закрытый ключ для шифрования сообщений
        /// </summary>
        [JsonProperty(PropertyName = "CloseKey")]
        public long CloseKey { get; set; }
        
        /// <summary>
        /// Открытый ключ для шифрования сообщений
        /// </summary>
        [JsonProperty(PropertyName = "OpenKey")]
        public long OpenKey { get; set; }

        /// <summary>
        /// Время создания ключа
        /// </summary>
        [JsonProperty(PropertyName = "TimeCreate")]
        public DateTime TimeCreate { get; private set; }

        public long n { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        private KeyMessageEncryptionModel()
        {

        }

        public object Clone()
        {
            return new KeyMessageEncryptionModel
            {
                CloseKey = this.CloseKey,
                OpenKey = this.OpenKey,
                TimeCreate = this.TimeCreate
            };
        }

        public static KeyMessageEncryptionModel GenerateNewKey(int p, int q)
        {
            long EulerFun = 0, e = 2, d;
            
            long n = p * q;
            EulerFun = (p - 1) * (q - 1);

            Random random = new Random(Guid.NewGuid().GetHashCode());

            long[] resultGCD = MessageEncryption.GCD(EulerFun, e);

            while (resultGCD[0] != 1)
            {
                e = random.Next(2, (int)EulerFun - 1);
                resultGCD = MessageEncryption.GCD(EulerFun, e);
            }

            if (resultGCD[2] < 0)
            {
                d = resultGCD[2] + EulerFun;
            }
            else
            {
                d = resultGCD[2];
            }

            return new KeyMessageEncryptionModel()
            {
                TimeCreate = DateTime.Now,
                CloseKey = (int)d,
                OpenKey = (int)e,
                n = n
            };
        }
    }
}

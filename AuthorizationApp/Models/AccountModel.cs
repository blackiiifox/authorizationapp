﻿using AuthorizationApp.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

namespace AuthorizationApp.Models
{
    public class AccountModel : INotifyPropertyChanged
    {

        /// <summary>
        /// Id аккаунта
        /// </summary>
        [JsonProperty(PropertyName = "ID")]
        public int ID { get; set; }

        /// <summary>
        /// Состояние блокировки аккаунта
        /// </summary>
        [JsonProperty(PropertyName = "IsBlocked")]
        public bool IsBlocked { get; set; }

        /// <summary>
        /// Состояние нового аккаунта(аккаунт был только что создан и это первый вход)
        /// </summary>
        [JsonProperty(PropertyName = "IsNewAccount")]
        public bool IsNewAccount { get; set; }

        /// <summary>
        /// Логин
        /// </summary>
        [JsonProperty(PropertyName = "Login")]
        public string Login
        { get; set; }

        /// <summary>
        /// ФИО пользователя
        /// </summary>
        [JsonProperty(PropertyName = "FIO")]
        public string FullName { get; set; }

        /// <summary>
        /// Название группы, которой пренадлежит пользователь
        /// </summary>
        [JsonProperty(PropertyName = "GroupName")]
        public string GroupName { get; set; }

        /// <summary>
        /// Пароли аккаунта
        /// </summary>
        [JsonProperty(PropertyName = "Passwords")]
        public ObservableCollection<PasswordModel> Passwords { get; set; }

        /// <summary>
        /// Пароли аккаунта
        /// </summary>
        [JsonProperty(PropertyName = "ListKeysMessagesEncryption")]
        public ObservableCollection<KeyMessageEncryptionModel> ListKeysMessagesEncryption { get; set; }

        [JsonIgnore]
        public KeyMessageEncryptionModel CurrentKeyMessagesEncryption
        {
            get
            {
                if(ListKeysMessagesEncryption == null)
                {
                    ListKeysMessagesEncryption = new ObservableCollection<KeyMessageEncryptionModel>();
                }

                return ListKeysMessagesEncryption.LastOrDefault();
            }
            set
            {
                if (value != null)
                {
                    ListKeysMessagesEncryption.Add(value);
                }
            }
        }


        [JsonIgnore]
        public string CurrentPassword
        {
            get
            {
                if (Passwords.Count == 0)
                    return null;
                return Passwords.Last().Password;
            }
            set
            {
                if (Login == "ADMIN")
                {
                    if (!IsNewAccount)
                    {
                        ConfigManager.GetInstance().CreateNewEncryptionPasswordsKey();
                    }
                }

                PasswordModel newPass = new PasswordModel
                {
                    TimeCreate = DateTime.Now,
                    Password = value
                };
                Passwords.Add(newPass);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public static AccountModel GetDefault()
        {
            AccountModel newAcc = new AccountModel()
            {
                FullName = "",
                IsBlocked = false,
                Login = "",
                IsNewAccount = true,
                Passwords = new ObservableCollection<PasswordModel>(),
                ListKeysMessagesEncryption = new ObservableCollection<KeyMessageEncryptionModel>(),
                GroupName = GroupModel.BaseGroups.FirstOrDefault(group => group.GroupPermission.Equals(PermissionType.Low)).GroupName // new ObservableCollection<GroupType> { GroupType.User}
            };

            return newAcc;
        }

        public static ObservableCollection<AccountModel> GetAccounts()
        {
            ObservableCollection<AccountModel> accountLoaded = new ObservableCollection<AccountModel>();
            bool adminState = false;

            foreach (var item in PathManager.GetAccountsFilesPath())
            {
                AccountModel loadedAcc = (AccountModel)JsonSerDer.LoadObject<AccountModel>(item);

                if (loadedAcc.Login == "ADMIN")
                {
                    adminState = true;
                }

                accountLoaded.Add(loadedAcc);
            }

            if (adminState == false)
            {
                AccountModel admin = GetDefault();

                admin.ID = 0;
                admin.Login = "ADMIN";
                admin.CurrentPassword = "Qwerty1";
                admin.GroupName = GroupModel.BaseGroups.FirstOrDefault(group => group.GroupPermission.Equals(PermissionType.High)).GroupName;
                // admin.Group = new ObservableCollection<GroupType>() { GroupType.Admin };
                AccountModel.SaveAccount(admin);
                accountLoaded.Add(admin);
            }

            var profilesSorted = accountLoaded.OrderBy(x => x.ID).ToList();
            return accountLoaded;
        }

        /// <summary>
        /// Сохранение аккаунта.
        /// </summary>
        /// <param name="profile"></param>
        public static void SaveAccount(AccountModel account)
        {
            JsonSerDer.SaveObject(account, PathManager.GetAccountPath(account.ID.ToString()));
        }

        /// <summary>
        /// Удаление профиля.
        /// </summary>
        public static void DeleteAccount(AccountModel account)
        {
            FileIOManager.DeleteFolder(PathManager.GetAccountFolderPath(account.ID.ToString()));
        }

        /// <summary>
        /// Поиск наличия соответсвующего параметра аккаунта.
        /// Используется при фильтрации аккаунтов по какой-либо строке.
        /// </summary>
        /// <param name="key">Строка для поиска по значению праметра.</param>
        /// <returns>true - если строка соответствует значению одного из параметров, false - если соответствие не найдено.</returns>
        public bool FindProperty(string key)
        {
            if (IsContained(this.Login, key))
            {
                return true;
            }
            if (IsContained(this.ID.ToString(), key))
            {
                return true;
            }
            if (IsContained(this.FullName, key))
            {
                return true;
            }

            return false;
        }
        private bool IsContained(string searchParameter, string key)
        {
            if (searchParameter != null)
            {
                if (searchParameter.ToLower().Contains(key))
                {
                    return true;
                }
            }
            return false;
        }

    }

    public class PasswordModel : INotifyPropertyChanged
    {
        /// <summary>
        /// Время создания пароля
        /// </summary>
        [JsonProperty(PropertyName = "TimeCreate")]
        public DateTime TimeCreate { get; set; }

        [JsonProperty(PropertyName = "Password")]
        private string EncryptionPassword { get; set; }
        /// <summary>
        /// Пароль
        /// </summary>
        // [JsonProperty(PropertyName = "Password")]
        [JsonIgnore]
        public string Password
        {
            get
            {
                EncryptionPasswordConfigModel EncryptionPasswordConfig = ConfigManager.GetInstance().GetPasswordEncryptionConfig(TimeCreate);

                return EncryptionPassword != null ?
                    PasswordEncryption.Dencryption(EncryptionPassword, EncryptionPasswordConfig.KeyRows, EncryptionPasswordConfig.KeyColumns)
                    : null;
            }
            set
            {
                EncryptionPasswordConfigModel EncryptionPasswordConfig = ConfigManager.GetInstance().GetPasswordEncryptionConfig(TimeCreate);

                EncryptionPassword = value != null ?
                    PasswordEncryption.Encryption(value, EncryptionPasswordConfig.KeyRows, EncryptionPasswordConfig.KeyColumns)
                    : null;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }

}


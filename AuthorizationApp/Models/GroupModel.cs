﻿using AuthorizationApp.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthorizationApp.Models
{
    public class GroupModel : INotifyPropertyChanged
    {
        /// <summary>
        /// Базовые группы среднего, низкого и высокого уровня доступа
        /// </summary>
        public static List<GroupModel> BaseGroups;

        /// <summary>
        /// Имя класса
        /// </summary>
        [JsonProperty(PropertyName = "GroupName")]
        public string GroupName { get; set; }

        /// <summary>
        /// Уровень доступа группы
        /// </summary>
        [JsonProperty(PropertyName = "GroupPermission")]
        public PermissionType GroupPermission { get; set; } = PermissionType.Low;

        /// <summary>
        /// Информация о ключах шифрования сообщений
        /// </summary>
        [JsonProperty(PropertyName = "KeyMessageEncryptions")]
        public List<KeyMessageInfoModel> KeyMessageEncryptions { get; set; } = new List<KeyMessageInfoModel>();

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Извлечение групп
        /// </summary>
        /// <returns></returns>
        public static ObservableCollection<GroupModel> GetGroups()
        {
            ObservableCollection<GroupModel> groupsLoaded = new ObservableCollection<GroupModel>();

            foreach (var item in PathManager.GetGroupsFilesPath())
            {
                GroupModel loadedCommand = (GroupModel)JsonSerDer.LoadObject<GroupModel>(item);
                groupsLoaded.Add(loadedCommand);
            }

            BaseGroups = new List<GroupModel>();
            BaseGroups = (List<GroupModel>)JsonSerDer.LoadObject<List<GroupModel>>(PathManager.GetDefaultGroupsFilePath());

            foreach(var baseGroup in BaseGroups)
            {
                if(!groupsLoaded.Where(group => group.GroupName == baseGroup.GroupName).Any())
                {
                    groupsLoaded.Add(baseGroup);
                    SaveGroup(baseGroup);
                }
            }

            return groupsLoaded;
        }

        /// <summary>
        /// Сохранение группы.
        /// </summary>
        /// <param name="profile"></param>
        public static void SaveGroup(GroupModel group)
        {
            JsonSerDer.SaveObject(group, PathManager.GetGroupPath(group.GroupName));
        }

        /// <summary>
        /// Удаление группы.
        /// </summary>
        public static void DeleteGroup(GroupModel group)
        {
            FileIOManager.DeleteFolder(PathManager.GetGroupFolderPath(group.GroupName));
        }
    }
}

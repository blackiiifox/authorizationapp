﻿using AuthorizationApp.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthorizationApp.Models
{

    public class DictionaryLoginModel: INotifyPropertyChanged, ICloneable
    {

        /// <summary>
        /// Заблокированные логины системы
        /// </summary>
        [JsonProperty(PropertyName = "BlockLogins")]
        public ObservableCollection<string> BlockedLogins { get; set; } = new ObservableCollection<string>();

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Сохранение словаря.
        /// </summary>
        /// <param name="profile"></param>
        public static void SaveDictionary(DictionaryLoginModel dictionary)
        {
            JsonSerDer.SaveObject(dictionary, PathManager.GetDictionaryLoginFilePath());
        }

        /// <summary>
        /// Загрузка словаря
        /// </summary>
        /// <returns></returns>
        public static DictionaryLoginModel LoadDictionary()
        {
            DictionaryLoginModel dictionary;

            if (File.Exists(PathManager.GetDictionaryLoginFilePath()))
            {
                dictionary = (DictionaryLoginModel)JsonSerDer.LoadObject<DictionaryLoginModel>(PathManager.GetDictionaryLoginFilePath());
            }
            else
            {
                dictionary = GetDefault();
                SaveDictionary(dictionary);
            }

            return dictionary;
        }

        /// <summary>
        /// Стандартный словарь
        /// </summary>
        /// <returns></returns>
        public static DictionaryLoginModel GetDefault()
        {
            DictionaryLoginModel newDic = new DictionaryLoginModel()
            {
                BlockedLogins = new ObservableCollection<string>() { "ADMIN" }
            };

            return newDic;
        }


        public object Clone()
        {
            DictionaryLoginModel dic = new DictionaryLoginModel();

            foreach(string login in this.BlockedLogins)
            {
                dic.BlockedLogins.Add(login);
            }

            return dic;
        }
    }

    public class DictionaryPasswordModel: INotifyPropertyChanged, ICloneable
    {

        /// <summary>
        /// Словарь заблокированных паролей системы
        /// </summary>
        [JsonProperty(PropertyName = "BlockPasswords")]
        public ObservableCollection<string> BlockedPasswords { get; set; } = new ObservableCollection<string>();

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Сохранение словаря.
        /// </summary>
        /// <param name="profile"></param>
        public static void SaveDictionary(DictionaryPasswordModel dictionary)
        {
            JsonSerDer.SaveObject(dictionary, PathManager.GetDictionaryPasswordFilePath());
        }

        /// <summary>
        /// Загрузка словаря
        /// </summary>
        /// <returns></returns>
        internal static DictionaryPasswordModel LoadDictionary()
        {
            DictionaryPasswordModel dictionary;

            if (File.Exists(PathManager.GetDictionaryPasswordFilePath()))
            {
                dictionary = (DictionaryPasswordModel)JsonSerDer.LoadObject<DictionaryPasswordModel>(PathManager.GetDictionaryPasswordFilePath());
            }
            else
            {
                dictionary = GetDefault();
                SaveDictionary(dictionary);
            }

            return dictionary;
        }

        /// <summary>
        /// Стандартный словарь
        /// </summary>
        /// <returns></returns>
        public static DictionaryPasswordModel GetDefault()
        {
            DictionaryPasswordModel newDic = new DictionaryPasswordModel()
            {
                BlockedPasswords = new ObservableCollection<string>() {  }
            };

            return newDic;
        }

        public object Clone()
        {
            DictionaryPasswordModel dic = new DictionaryPasswordModel();

            foreach (string pass in this.BlockedPasswords)
            { 
                dic.BlockedPasswords.Add(pass);
            }

            return dic;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AuthorizationApp.Services;
using Newtonsoft.Json;

namespace AuthorizationApp.Models
{
    public class ConfigPasswordModel : INotifyPropertyChanged, ICloneable
    {
        [JsonProperty(PropertyName = "DictionaryCheckEnable")]
        public bool DictionaryCheckEnable { get; set; }

        [JsonProperty(PropertyName = "MinLimiter")]
        public PasswordTimeLimiterModel MinLimiter { get; set; }

        [JsonProperty(PropertyName = "MaxLimiter")]
        public PasswordTimeLimiterModel MaxLimiter { get; set; }

        [JsonProperty(PropertyName = "LogsEnable")]
        public bool LogsEnable { get; set; }

        [JsonProperty(PropertyName = "AutoGenerateEnable")]
        public bool AutoGenerateEnable { get; set; }

        [JsonProperty(PropertyName = "ForcedChangeEnable")]
        public bool ForcedChangeEnable { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public object Clone()
        {
            return new ConfigPasswordModel
            {
                DictionaryCheckEnable = this.DictionaryCheckEnable,
                MinLimiter = (PasswordTimeLimiterModel)this.MinLimiter.Clone(),
                MaxLimiter = (PasswordTimeLimiterModel)this.MaxLimiter.Clone(),
                LogsEnable = this.LogsEnable,
                AutoGenerateEnable = this.AutoGenerateEnable,
                ForcedChangeEnable = this.ForcedChangeEnable
            };
        }

        internal static ConfigPasswordModel LoadConfig()
        {
            ConfigPasswordModel loadedCommand;

            if (File.Exists(PathManager.GetConfigPasswordFilePath()))
            {
                loadedCommand = (ConfigPasswordModel)JsonSerDer.LoadObject<ConfigPasswordModel>(PathManager.GetConfigPasswordFilePath());
            }
            else
            {
                loadedCommand = GetDefault();
                SaveConfigPassword(loadedCommand);
            }

            return loadedCommand;
        }

        public static ConfigPasswordModel GetDefault()
        {
            ConfigPasswordModel newCnf = new ConfigPasswordModel()
            {
                DictionaryCheckEnable = false,
                MinLimiter = PasswordTimeLimiterModel.GetDefault(),
                MaxLimiter = PasswordTimeLimiterModel.GetDefault(),
                LogsEnable = false,
                AutoGenerateEnable = false,
                ForcedChangeEnable = false
            };

            return newCnf;
        }

        /// <summary>
        /// Сохранение конфигурации паролей.
        /// </summary>
        /// <param name="profile"></param>
        public static void SaveConfigPassword(ConfigPasswordModel cnf)
        {
            JsonSerDer.SaveObject(cnf, PathManager.GetConfigPasswordFilePath());
        }

    }

    public class PasswordTimeLimiterModel : INotifyPropertyChanged, ICloneable
    {

        [JsonProperty(PropertyName = "TimeLimit")]
        public TimeSpan TimeLimit { get; set; }

        [JsonProperty(PropertyName = "TimeLimitEnable")]
        public bool TimeLimitEnable { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public object Clone()
        {
            return new PasswordTimeLimiterModel
            {
                TimeLimit = this.TimeLimit,
                TimeLimitEnable = this.TimeLimitEnable
            };
        }

        public static PasswordTimeLimiterModel GetDefault()
        {
            PasswordTimeLimiterModel newLim = new PasswordTimeLimiterModel()
            {
                TimeLimitEnable = false,
                TimeLimit = new TimeSpan(0)
            };

            return newLim;
        }
    }
}

﻿using AuthorizationApp.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthorizationApp.Models
{

    public class EncryptionConfigModel : INotifyPropertyChanged
    {
        /// <summary>
        /// Текущия конфигурация шифрования паролей
        /// </summary>
        public ObservableCollection<EncryptionPasswordConfigModel> HistoryEncryptionPasswordConfig { get; set; } 

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Сохранение конфигурация.
        /// </summary>
        /// <param name="profile"></param>
        public static void Save(EncryptionConfigModel config)
        {
            JsonSerDer.SaveObject(config, PathManager.GetEncryptionConfigFilePath());
        }

        /// <summary>
        /// Загрузка конфигурации.
        /// </summary>
        /// <param name="profile"></param>
        public static EncryptionConfigModel Load()
        {
            EncryptionConfigModel Config;

            if (File.Exists(PathManager.GetEncryptionConfigFilePath()))
            {
                Config = (EncryptionConfigModel)JsonSerDer.LoadObject<EncryptionConfigModel>(PathManager.GetEncryptionConfigFilePath());
            }
            else
            {
                Config = GetDefault();
                Save(Config);
            }

            return Config;
        }

        public static EncryptionConfigModel GetDefault()
        {
            return new EncryptionConfigModel
            {
                HistoryEncryptionPasswordConfig = new ObservableCollection<EncryptionPasswordConfigModel>() { EncryptionPasswordConfigModel.GetDefault() }
            };
        }
    }

    public class EncryptionPasswordConfigModel : INotifyPropertyChanged
    {
        /// <summary>
        /// Время создания ключей
        /// </summary>
        [JsonProperty(PropertyName = "TimeCreate")]
        public DateTime TimeCreate { get; set; }

        /// <summary>
        /// Ключ для шифровки столбцов
        /// </summary>
        [JsonProperty(PropertyName = "KeyColumns")]
        public string KeyColumns { get; set; }


        /// <summary>
        /// Ключ для шифровки строк
        /// </summary>
        [JsonProperty(PropertyName = "KeyRows")]
        public string KeyRows { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public static EncryptionPasswordConfigModel GetDefault()
        {
            return new EncryptionPasswordConfigModel
            {
                TimeCreate = DateTime.Now,
                KeyColumns = PasswordEncryption.RandomString(5),
                KeyRows = PasswordEncryption.RandomString(5)
            };

        }
    }
}

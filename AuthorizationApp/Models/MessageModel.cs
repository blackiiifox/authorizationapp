﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AuthorizationApp.Services;
using static AuthorizationApp.Services.Binary;

namespace AuthorizationApp.Models
{
    /// <summary>
    /// Класс сообщений, которыми оперируют пользователи системы.
    /// </summary>
    class MessageModel : INotifyPropertyChanged
    {
        /// <summary>
        /// Время отправки сообщения
        /// </summary>
        public DateTime TimeSend { get; set; } = DateTime.Now;
    
        /// <summary>
        /// Зашифрованное сообщение
        /// </summary>
        public long[] EncryptionMessage { get; set; }

        /// <summary>
        /// Сообщение
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Отправитель
        /// </summary>
        public string SenderLogin { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public static void Save(MessageModel message , string path)
        {
            ObjectToByteConverter ByteConverter = new ObjectToByteConverter();
            // byte[] data = ByteConverter.Convert(message, null, null, null) as byte[];

            Writer writer = new Writer();

            List<object> arrayObjects = new List<object>
            {
                message.TimeSend,
                message.SenderLogin,
                message.EncryptionMessage.Length
            };
            foreach (long encrtyptionCode in message.EncryptionMessage)
            {
                arrayObjects.Add(encrtyptionCode);
            }

            byte[] data = writer.GetRawParams(arrayObjects.ToArray());

            File.WriteAllBytes(path, data.ToArray());
        }

        public static MessageModel Load(string path)
        {
            MessageModel message = new MessageModel();
            ObjectToByteConverter ByteConverter = new ObjectToByteConverter();

            // byte[] data = FileIOManager.ReadByteData(path);
            byte[] data =  File.ReadAllBytes(path);
            // ByteConverter.ConvertBack(data, null, message, null);

            Reader reader = new Reader((byte[])data);

            message.TimeSend = reader.ReadDateTime();
            message.SenderLogin = reader.ReadString();
            int lenghtData = reader.ReadInt();

            message.EncryptionMessage = new long[lenghtData];

            for (int i = 0; i < lenghtData; i++)
            {
                message.EncryptionMessage[i] = reader.ReadLong();
            }

            return message;
        }
    }
}

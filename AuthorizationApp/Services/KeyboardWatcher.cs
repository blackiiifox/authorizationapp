﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Runtime.InteropServices;
using static AuthorizationApp.ViewModels.AccessWindowViewModel;

namespace AuthorizationApp.Services
{
    class KeyboardWatcher
    {
        public static int IdRussianLayout = 1033;
        private readonly Timer _timer;
        private int _currentLayout = IdRussianLayout;
        private bool _currentCaps;

        public KeyboardLayoutChanged KeyboardLayoutChanged;
        public KeyboardCapsChanged KeyboardCapsChanged;

        public KeyboardWatcher()
        {
            _timer = new Timer(new TimerCallback(CheckKeyboardLayout), null, TimeSpan.FromSeconds(0.03), TimeSpan.FromSeconds(0.03));
        }

        [DllImport("user32.dll")] static extern IntPtr GetForegroundWindow();
        [DllImport("user32.dll")] static extern uint GetWindowThreadProcessId(IntPtr hwnd, IntPtr proccess);
        [DllImport("user32.dll")] static extern IntPtr GetKeyboardLayout(uint thread);
        public int GetCurrentKeyboardLayout()
        {
            try
            {
                IntPtr foregroundWindow = GetForegroundWindow();
                uint foregroundProcess = GetWindowThreadProcessId(foregroundWindow, IntPtr.Zero);
                int keyboardLayout = GetKeyboardLayout(foregroundProcess).ToInt32() & 0xFFFF;

                if (keyboardLayout == 0)
                {
                    // something has gone wrong - just assume Russian
                    keyboardLayout = IdRussianLayout;
                }
                return keyboardLayout;
            }
            catch (Exception ex)
            {
                // if something goes wrong - just assume Russian
                return IdRussianLayout;
            }
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        public static extern short GetKeyState(int keyCode);

        private void CheckKeyboardLayout(object sender)
        {
            var layout = GetCurrentKeyboardLayout();
            if (_currentLayout != layout && KeyboardLayoutChanged != null)
            {
                KeyboardLayoutChanged(_currentLayout, layout);
                _currentLayout = layout;
            }

            bool stateCaps = (((ushort)GetKeyState(0x14)) & 0xffff) != 0;
            if (_currentCaps != stateCaps && KeyboardCapsChanged != null)
            {
                KeyboardCapsChanged(_currentCaps, stateCaps);
                _currentCaps = stateCaps;
            }

        }

        private void ReleaseUnmanagedResources()
        {
            _timer.Dispose();
        }

        public void Dispose()
        {
            ReleaseUnmanagedResources();
            GC.SuppressFinalize(this);
        }

        ~KeyboardWatcher()
        {
            ReleaseUnmanagedResources();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AuthorizationApp.Services
{
    class PasswordEncryption
    {
        public static string Encryption(string password, string keyRows, string keyColumns)
        {
            char[,] CharArray = CreateEncryptionTable(password, keyColumns.Count(), keyRows.Count());

            // Преобразуем ключи 
            int[] KeyRowsArray = KeyConverter(keyRows);
            int[] KeyColumnsArray = KeyConverter(keyColumns);

            // Cортировка по стобцам
            int[] helperColumns = KeyColumnsArray.OrderBy(i => i).ToArray().Clone() as int[];

            for (int i = 0; i < helperColumns.Count(); i++)
            {
                for (int j = 0; j < KeyColumnsArray.Length; j++)
                {
                    if (KeyColumnsArray[j] == helperColumns[i])
                    {
                        ArraySwapHelper.SwapColumns<char>(ref CharArray, j, i);
                        ArraySwapHelper.Swap<int>(ref KeyColumnsArray, i, j);
                    }
                }
            }


            // Сортировка по строкам
            int[] helperRows = KeyRowsArray.OrderBy(i => i).ToArray().Clone() as int[];

            for (int i = 0; i < helperRows.Count(); i++)
            {
                for (int j = 0; j < KeyRowsArray.Length; j++)
                {
                    if (KeyRowsArray[j] == helperRows[i])
                    {
                        ArraySwapHelper.SwapRows<char>(ref CharArray, j, i);
                        ArraySwapHelper.Swap<int>(ref KeyRowsArray, i, j);
                    }
                }
            }

            return TableToString(CharArray);
        }

        public static string Dencryption(string encryptionPassword, string keyRows, string keyColumns)
        {
            // ArraySwapHelper.DebugPrint<char>(ref CharArray);

            char[,] CharArray = CreateEncryptionTable(encryptionPassword, keyColumns.Count(), keyRows.Count(), false);

            // Преобразуем ключи 
            int[] KeyRowsArray = KeyConverter(keyRows);
            int[] KeyColumnsArray = KeyConverter(keyColumns);


            // Обратная сортировка по строкам
            int[] helperRows = KeyRowsArray.OrderBy(i => i).ToArray().Clone() as int[];

            for (int i = 0; i < KeyRowsArray.Length; i++)
            {
                for (int j = 0; j < helperRows.Count(); j++)
                {
                    if (KeyRowsArray[i] == helperRows[j])
                    {
                        ArraySwapHelper.SwapRows<char>(ref CharArray, j, i);
                        ArraySwapHelper.Swap<int>(ref helperRows, j, i);
                    }
                }
            }

            // Обратная cортировка по стобцам
            int[] helperColumns = KeyColumnsArray.OrderBy(i => i).ToArray().Clone() as int[];

            for (int i = 0; i < KeyColumnsArray.Length; i++)
            {
                for (int j = 0; j < helperColumns.Count(); j++)
                {
                    if (KeyColumnsArray[i] == helperColumns[j])
                    {
                        ArraySwapHelper.SwapColumns<char>(ref CharArray, j, i);
                        ArraySwapHelper.Swap<int>(ref helperColumns, i, j);
                    }
                }
            }

            return TableToString(CharArray, false);
        }

        private static int[] KeyConverter(string key)
        {
            int[] valueKey = new int[key.Length];

            for (int i = 0; i < key.Length; i++)
            {
                valueKey[i] = Convert.ToInt32(key[i]);
            }

            var helper = valueKey.OrderBy(i => i);

            int baseNumber = 1;
            foreach (var item in helper)
            {
                for (int i = 0; i < valueKey.Length; i++)
                {
                    if (valueKey[i] == item)
                    {
                        valueKey[i] = baseNumber;
                        baseNumber++;
                        break;
                    }
                }
            }

            return valueKey;
        }

        private static string TableToString(char[,] table, bool IsEncryption = true)
        {
            string result = "";
            if (IsEncryption)
            {
                for (int i = 0; i < table.GetLength(1); i++)
                {
                    for (int j = 0; j < table.GetLength(0); j++)
                    {
                        result += table[j, i] != '\0' ? table[j, i].ToString() : "\x255";
                    }
                }
            }
            else
            {
                // ArraySwapHelper.DebugPrint<char>(ref table);
                for (int i = 0; i < table.GetLength(0); i++)
                {
                    for (int j = 0; j < table.GetLength(1); j++)
                    {
                        result += table[i, j] != '\0' ? table[i, j].ToString() : "";
                    }
                }
            }

            return result;
        }

        private static char[,] CreateEncryptionTable(string password, int rows, int columns, bool IsEncryption = true)
        {
            char[,] arrayChar = new char[rows, columns];

            if (IsEncryption)
            {
                for (int i = 0; i < rows; i++)
                {
                    for (int j = 0; j < columns; j++)
                    {
                        int newIndexPassword = (columns * i) + j;
                        arrayChar[i, j] = newIndexPassword < password.Length ? password[newIndexPassword] : '\0';
                    }
                }
            }
            else
            {
                for (int i = 0; i < columns; i++)
                {
                    for (int j = 0; j < rows; j++)
                    {
                        int newIndexPassword = (columns * i) + j;
                        arrayChar[j, i] = newIndexPassword < password.Length ? password[newIndexPassword] != '\x255' ? password[newIndexPassword] : '\0' : '\0';
                    }
                }
            }

            return arrayChar;
        }

        public static string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random(Guid.NewGuid().GetHashCode());
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            return builder.ToString();
        }
    }

    class ArraySwapHelper
    {
        public static void Swap<T>(ref T[,] inputs, int row1, int column1, int row2, int column2)
        {
            var tmp = inputs[row1, column1];
            inputs[row1, column1] = inputs[row2, column2];
            inputs[row2, column2] = tmp;
        }

        public static void Swap<T>(ref T[] inputs, int i, int j)
        {
            var tmp = inputs[i];
            inputs[i] = inputs[j];
            inputs[j] = tmp;
        }

        public static void SwapColumns<T>(ref T[,] inputs, int column1, int column2)
        {
            for (int indexRow = 0; indexRow < inputs.GetLength(0); indexRow++)
            {
                Swap<T>(ref inputs, indexRow, column1, indexRow, column2);
            }
        }

        public static void SwapRows<T>(ref T[,] inputs, int row1, int row2)
        {
            for (int indexColumn = 0; indexColumn < inputs.GetLength(1); indexColumn++)
            {
                Swap<T>(ref inputs, row1, indexColumn, row2, indexColumn);
            }
        }

        public static void DebugPrint<T>(ref T[,] inputs)
        {
            string info = "ArrayHelper.DebugPrint: Rows = " + inputs.GetLength(0) + ", Columns = " + inputs.GetLength(1);
            Debug.WriteLine(info);
            for (int indexRow = 0; indexRow < inputs.GetLength(0); indexRow++)
            {
                string newString = "";
                for (int indexColumn = 0; indexColumn < inputs.GetLength(1); indexColumn++)
                {
                    newString += Convert.ToString(inputs[indexRow, indexColumn]) + " ";
                    newString = newString.Replace("\0", @"\0");
                }
                newString += "\n";
                Debug.WriteLine(newString);
            }
            Debug.WriteLine("\n");
        }


    }
}

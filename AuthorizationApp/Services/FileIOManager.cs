﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace AuthorizationApp.Services
{
    class FileIOManager
    {
        public static string ReadData(string path)
        {
            string str = "";
            try
            {
                using (StreamReader rdr = new StreamReader(path))
                {
                    str = rdr.ReadToEnd();
                }
                return str;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
                throw;
            }

        }

        private static bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                if(file.Exists)
                {
                    stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
                }
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        public static byte[] ReadByteData(string path)
        {
            byte[] buffer = null;

            // while (IsFileLocked(new FileInfo(path))) ;

            using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                if (stream.Length > 0)
                {
                    buffer = new byte[stream.Length];
                    stream.Read(buffer, 0, (int)stream.Length);
                    // stream.Close();
                }
            }
            return buffer;
        }

        public static string WriteData(string path, string data, bool Append = false)
        {
            using (StreamWriter wrt = new StreamWriter(path, Append))
            {
                wrt.Write(data);
            }
            return data;
        }

        public static bool WriteData(string path, byte[] data, bool Append = false)
        {
            try
            {
                // while (IsFileLocked(new FileInfo(path))) ;

                using (var stream = new FileStream(path, FileMode.Create, FileAccess.Write))
                {
                    stream.Write(data, 0, data.Length);
                    // stream.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static void DeleteFolder(string path)
        {
            Directory.Delete(path, true);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthorizationApp.Services
{
    public class Binary
    {
        public class Writer
        {
            public byte[] GetRawParams(object[] Objs)
            {
                List<byte> bytes = new List<byte>();
                if (Objs != null)
                {
                    foreach (var item in Objs)
                    {
                        if (item is byte)
                        {
                            bytes.Add((byte)item);
                        }
                        else if (item is float)
                        {
                            bytes.AddRange(BitConverter.GetBytes((float)item));
                        }
                        else if (item is long)
                        {
                            bytes.AddRange(BitConverter.GetBytes((long)item));
                        }
                        else if (item is double)
                        {
                            bytes.AddRange(BitConverter.GetBytes((double)item));
                        }
                        else if (item is short)
                        {
                            bytes.AddRange(BitConverter.GetBytes((short)item));
                        }
                        else if (item is int)
                        {
                            bytes.AddRange(BitConverter.GetBytes((int)item));
                        }
                        else if (item is UInt64)
                        {
                            bytes.AddRange(BitConverter.GetBytes((UInt64)item));
                        }
                        else if (item is UInt16)
                        {
                            bytes.AddRange(BitConverter.GetBytes((UInt16)item));
                        }
                        else if (item is string)
                        {
                            var bts = Encoding.Unicode.GetBytes((string)item);
                            bytes.AddRange(BitConverter.GetBytes((ushort)bts.Length));
                            bytes.AddRange(bts);
                        }
                        else if (item is ushort)
                        {
                            bytes.AddRange(BitConverter.GetBytes((ushort)item));
                        }
                        else if (item is DateTime)
                        {
                            bytes.AddRange(BitConverter.GetBytes(((DateTime)item).Ticks));
                        }
                    }
                }
                return bytes.ToArray();
            }
            public byte[] GetCompletePacket(byte Tag, object[] Objs, bool InsertPacketLenght)
            {
                List<byte> bytes = new List<byte>();
                bytes.Add(Tag);
                bytes.AddRange(GetRawParams(Objs));
                if (InsertPacketLenght)
                {
                    bytes.InsertRange(0, BitConverter.GetBytes((ushort)bytes.Count));
                }
                return bytes.ToArray();
            }
        }
        public class Reader
        {
            public byte[] bytes;
            private int position = 0;

            public byte[] GetBytes(int Len)
            {
                byte[] outer = new byte[Len];
                try
                {
                    Array.Copy(bytes, position, outer, 0, Len);
                    position += Len;
                }
                catch (Exception ex)
                {
                    throw new Exception("Len is greater than count", ex);
                }
                return outer;
            }
            public int Lenght()
            {
                return bytes.Length;
            }
            public Reader(byte[] Bytes)
            {
                bytes = Bytes;
                position = 0;
            }
            public Reader(byte[] Bytes, int Limit)
            {
                bytes = new byte[Limit];
                Array.Copy(Bytes, bytes, Limit);
                position = 0;
            }
            public int Remained()
            {
                return bytes.Length - position;
            }
            public byte ReadByte()
            {
                var outer = bytes[position];
                position++;
                return outer;
            }
            public int ReadInt()
            {
                var outer = BitConverter.ToInt32(bytes, position);
                position += 4;
                return outer;
            }
            public short ReadShort()
            {
                var outer = BitConverter.ToInt16(bytes, position);
                position += 2;
                return outer;
            }
            public ushort ReadUShort()
            {
                var outer = BitConverter.ToUInt16(bytes, position);
                position += 2;
                return outer;
            }
            public UInt64 ReadUlong()
            {
                var outer = BitConverter.ToUInt64(bytes, position);
                position += 8;
                return outer;
            }
            public long ReadLong()
            {
                var outer = BitConverter.ToInt64(bytes, position);
                position += 8;
                return outer;
            }
            public float ReadFloat()
            {
                var outer = BitConverter.ToSingle(bytes, position);
                position += 4;
                return outer;
            }
            public double ReadDouble()
            {
                var outer = BitConverter.ToDouble(bytes, position);
                position += 8;
                return outer;
            }
            public DateTime ReadDateTime()
            {
                var outer = new DateTime(BitConverter.ToInt64(bytes, position));
                position += 8;
                return outer;
            }

            public string ReadString()
            {
                ushort lenght = BitConverter.ToUInt16(bytes, position);
                position += 2;
                var outer = Encoding.Unicode.GetString(bytes, position, lenght);
                position += lenght;
                return outer;
            }
        }
    }
}

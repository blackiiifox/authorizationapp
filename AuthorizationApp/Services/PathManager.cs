﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthorizationApp.Services
{
    class PathManager
    {
        public static string MainDataPath = System.AppDomain.CurrentDomain.BaseDirectory + "\\Data\\";
        private static readonly string AccountsFolderName = "Accounts\\";
        private static readonly string DictionaryFolderName = "Dictionary\\";
        private static readonly string GroupsFolderName = "Groups\\";
        private static readonly string AccountFileName = "account.json";
        private static readonly string GroupFileName = "group.json";
        private static readonly string DictionaryPasswordFile = "dictionary_password.json";
        private static readonly string DictionaryLoginFile = "dictionary_login.json";
        private static readonly string ConfigFolderName = "Config\\";
        private static readonly string MessagesFolderName = "Messages\\";
        private static readonly string GeneralConfigFileName = "config.json";
        private static readonly string DefaultGroupsFileName = "DefaultGroups.json";
        private static readonly string EncryptionConfigFileName = "EncryptionConfig.json";

        private static readonly string ExtensionMessageFile = ".message";

        public static string GetConfigFolderPath()
        {
            string path = MainDataPath + ConfigFolderName;

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            return path;

        }

        internal static string GetEncryptionConfigFilePath()
        {
            string path = GetConfigFolderPath() + EncryptionConfigFileName;

            return path;
        }

        public static string GetDefaultGroupsFilePath()
        {
            string path = GetConfigFolderPath() + DefaultGroupsFileName;

            if(!File.Exists(path))
            {
                File.WriteAllBytes(path, ProgramResources.DefaultGroups);
            }

            return path;

        }

        public static string GetConfigPasswordFilePath()
        {
            string path = GetConfigFolderPath() + GeneralConfigFileName;
            return path;

        }

        public static string GetAccountsFolderPath()
        {
            string path = MainDataPath + AccountsFolderName;

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            return path;

        }

        public static List<string> GetAccountsFilesPath()
        {
            List<string> commands = new List<string>();
            string startPath = GetAccountsFolderPath();
            string[] dirs = Directory.GetDirectories(startPath);
            foreach (var item in dirs)
            {
                string p = item + "\\" + AccountFileName;
                if (File.Exists(p))
                {
                    commands.Add(p);
                }
            }
            return commands;
        }

        public static string GetAccountFolderPath(string ID)
        {
            string path = GetAccountsFolderPath() + ID + "\\";

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            return path;
        }

        public static string GetAccountPath(string ID)
        {
            string path = GetAccountFolderPath(ID) + AccountFileName;
            return path;
        }

        public static List<string> GetGroupsFilesPath()
        {
            List<string> groups = new List<string>();
            string startPath = GetGroupsFolderPath();
            string[] dirs = Directory.GetDirectories(startPath);
            foreach (var item in dirs)
            {
                string p = item + "\\" + GroupFileName;
                if (File.Exists(p))
                {
                    groups.Add(p);
                }
            }
            return groups;
        }

        public static List<string> GetMessagesFilesPath(string IdAccount)
        {
            List<string> messagesPath = new List<string>();
            string path = MainDataPath + AccountsFolderName + IdAccount + "\\";
            if (Directory.Exists(path))
            {
                path += MessagesFolderName;

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                string[] files = Directory.GetFiles(path, "*" + ExtensionMessageFile);
                foreach (var item in files)
                {
                    messagesPath.Add(item);

                }
            }

            return messagesPath;
        }

        public static string GetGroupsFolderPath()
        {
            string path = MainDataPath + GroupsFolderName;

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            return path;

        }

        public static string GetGroupFolderPath(string NameGroup)
        {
            string path = GetGroupsFolderPath() + NameGroup + "\\";

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            return path;
        }

        public static string GetGroupPath(string NameGroup)
        {
            string path = GetGroupFolderPath(NameGroup) + GroupFileName;
            return path;
        }

        public static string GetDictionaryFolderPath()
        {
            string path = MainDataPath + DictionaryFolderName;

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            return path;
        }

        public static string GetDictionaryPasswordFilePath()
        {
            string path = GetDictionaryFolderPath() + DictionaryPasswordFile;
            return path;
        }

        public static string GetDictionaryLoginFilePath()
        {
            string path = GetDictionaryFolderPath() + DictionaryLoginFile;
            return path;
        }

        public static string GetMessageFilePath(string IdAccount)
        {
            string path = MainDataPath + AccountsFolderName + IdAccount + "\\" + MessagesFolderName;
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            path += Guid.NewGuid().ToString() + ExtensionMessageFile;

            return path;
        }
    }
}

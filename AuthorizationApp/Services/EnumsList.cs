﻿using System.ComponentModel;

namespace AuthorizationApp.Services
{
    /// <summary>
    /// Перечисление групп аккаунтов
    /// </summary>
    public enum GroupType {[Description("Не выбрана")] Empty, [Description("Администратор")] Admin, [Description("Пользователь")] User }

    public enum PermissionType {[Description("Низкий уровень доступа")] Low, [Description("Средний уровень доступа")] Medium, [Description("Высокий уровень доступа")] High }
}

﻿using AuthorizationApp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace AuthorizationApp.Services
{

    /// <summary>
    /// Синглетный класс для обработки данных, получаемых из файловой системы.
    /// </summary>
    public class DataManager : INotifyPropertyChanged
    {
        #region Variables
        // Реализация синглтона
        public event PropertyChangedEventHandler PropertyChanged;
        private static DataManager instance;
        private static readonly object syncRoot = new object();

        public ConfigPasswordModel ConfigPassord { get; set; }
        public DictionaryPasswordModel DictionaryPassword { get; set; }
        public DictionaryLoginModel DictionaryLogin { get; set; }
        public ObservableCollection<AccountModel> AccountsList { get; set; }
        public ObservableCollection<GroupModel> GroupsList { get; set; }
        #endregion

        #region Constructors
        private DataManager()
        {
            GroupsList = GroupModel.GetGroups();
            ConfigPassord = ConfigPasswordModel.LoadConfig();
            DictionaryPassword = DictionaryPasswordModel.LoadDictionary();
            DictionaryLogin = DictionaryLoginModel.LoadDictionary();
            AccountsList = AccountModel.GetAccounts();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Метод для получения объекта менеджера данных 
        /// </summary>
        /// <returns></returns>
        public static DataManager GetInstance()
        {
            if (instance == null)
            {
                lock (syncRoot)
                {
                    if (instance == null)
                        instance = new DataManager();
                }
            }
            return instance;
        }

        /// <summary>
        /// Регистрация нового аккаунта.
        /// </summary>
        /// <param name="newAccount"></param>
        public bool CreateNewAccount(AccountModel newAccount)
        {
            if (CheckAccount(newAccount))
            {
                newAccount.ID = GetNewID();
                AccountsList.Add(newAccount);
                AccountModel.SaveAccount(newAccount);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Проверяет аккаунт на соответсвие требованиям регистрации
        /// </summary>
        /// <param name="newAccount"></param>
        /// <returns></returns>
        private bool CheckAccount(AccountModel newAccount)
        {
            foreach (AccountModel acc in AccountsList)
            {
                if (acc.Login == newAccount.Login)
                {
                    WindowManager.ShowMessagebox("SuchAccountExists");
                    return false;
                }
            }

            // Проверка на словарь
            foreach (string loginBlock in DictionaryLogin.BlockedLogins)
            {
                if (loginBlock == newAccount.Login)
                {
                    WindowManager.ShowMessagebox("LoginIsBlockedInSystem");
                    return false;
                }
            }

            if (!CheckPassword(newAccount))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Проверяет пароль на соответсвие требованиям
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        private bool CheckPassword(AccountModel account)
        {

            if (ConfigPassord.LogsEnable)
            {
                foreach (PasswordModel password in account.Passwords)
                {
                    if (password.Password == account.CurrentPassword)
                    {
                        if (!(account.Passwords.IndexOf(password) == account.Passwords.Count - 1))
                        {
                            WindowManager.ShowMessagebox("PasswordHasAlreadyBeenRegistered");
                            account.Passwords.Remove(password);
                            return false;
                        }
                    }
                }
            }

            if (ConfigPassord.DictionaryCheckEnable)
            {
                foreach (string passwordBlock in DictionaryPassword.BlockedPasswords)
                {
                    if (passwordBlock == account.CurrentPassword)
                    {
                        WindowManager.ShowMessagebox("PasswordIsBlockedInSystem");
                        account.Passwords.Remove(account.Passwords.Last());
                        return false;
                    }
                }
            }

            return true;
        }

        public bool PasswordChange(AccountModel account, PasswordModel password)
        {
            if (CheckPassword(account))
            {
                if (account.CurrentPassword == password.Password)
                {
                    WindowManager.ShowMessagebox((string)Application.Current.Resources["PasswordChangeExistError"] + " " + password.Password + ".", false);
                    return false;
                }

                // Если это первый вход и включена смена пароля при первом входе
                if (ConfigPassord.ForcedChangeEnable && account.IsNewAccount)
                {
                    account.IsNewAccount = false;
                    account.CurrentPassword = password.Password;
                    AccountModel.SaveAccount(account);
                    return true;
                }
                else
                {
                    // Если включена проверка на минимальное время жизни пароля
                    if (ConfigPassord.MinLimiter.TimeLimitEnable)
                    {
                        if (!(ConfigPassord.MinLimiter.TimeLimit.Minutes == 0 && ConfigPassord.MinLimiter.TimeLimit.Hours == 0 && ConfigPassord.MinLimiter.TimeLimit.Days == 0))
                        {
                            if (account.Passwords.Count > 1)
                            {
                                if (account.Passwords.Last().TimeCreate + ConfigPassord.MinLimiter.TimeLimit > DateTime.Now)
                                {
                                    WindowManager.ShowMessagebox("PasswordMinTimeLimit");
                                    return false;
                                }
                            }
                        }
                        
                    }
                    account.CurrentPassword = password.Password;
                    AccountModel.SaveAccount(account);
                    return true;
                }
            }
            else
            {
                return false;
            }

        }

        private int GetNewID()
        {
            int newID = 1;
            foreach (AccountModel acc in AccountsList)
            {
                if (acc.ID >= newID)
                    newID = acc.ID + 1;
            }
            return newID;
        }

        public AccountModel GetAccount(string login)
        {
            AccountModel retAcc = null;

            foreach (AccountModel acc in AccountsList)
            {
                if (acc.Login == login)
                    retAcc = acc;
            }
            return retAcc;
        }

        internal void DeleteAccount(AccountModel selectedAccount)
        {
            AccountModel.DeleteAccount(selectedAccount);
            AccountsList.Remove(selectedAccount);
        }
        #endregion
    }
}

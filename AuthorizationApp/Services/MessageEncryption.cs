﻿using AuthorizationApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace AuthorizationApp.Services
{

    class MessageEncryption
    { 
        public static long[] Encryption(string message, KeyMessageInfoModel key)
        {
            char[] msg = message.ToCharArray();
            byte[] bytes = Encoding.GetEncoding(1251).GetBytes(msg);

            long[] codes = new long[bytes.Length];
            for (int i = 0; i < bytes.Length; i++)   
            {
                codes[i] = Convert.ToInt64(bytes[i]);          
            }

            string[] encryptionMessage = new string[codes.Length];

            for (int i = 0; i < codes.Length; i++)
            {
                // var result = FEA(codes[i], key.OpenKey, key.n);
                codes[i] = FEA(codes[i], key.OpenKey, key.n);
                // encryptionMessage[i] =  Convert.ToString(result, 2);
            }

            return codes;
        }

        public static string Decryption(long[] encryptionMessage, KeyMessageEncryptionModel key)
        {
            long[] codes = new long[encryptionMessage.Length];
            byte[] bytes = new byte[encryptionMessage.Length];

            for (int i = 0; i < encryptionMessage.Length; i++)
            {
                codes[i] = FEA(encryptionMessage[i], key.CloseKey, key.n);

                byte[] bytesTmp = BitConverter.GetBytes(codes[i]);
                if (BitConverter.IsLittleEndian)
                {
                    Array.Reverse(bytesTmp);
                }
                bytes[i] = bytesTmp[7];
            }
            
            Encoding win1251 = Encoding.GetEncoding(1251);
            Encoding utf8 = Encoding.UTF8;
            byte[] utf8Bytes = Encoding.Convert(win1251, utf8, bytes);
            string message = Encoding.UTF8.GetString(utf8Bytes);

            return message;
        }


        private static int Calculate_d(int m)
        {
            int d = m - 1;

            for (int i = 2; i <= m; i++)
                if ((m % i == 0) && (d % i == 0)) //если имеют общие делители
                {
                    d--;
                    i = 1;
                }

            return d;
        }


        /// <summary>
        /// Получение рандомного простого числа.
        /// </summary>
        /// <returns></returns>
        public static int GetPrime()
        {
            // Random random = new Random(Guid.NewGuid().GetHashCode());
            while (true)
            {
                //int randomValue = random.Next(100, 99999);
                int randomValue = GetRandomValue(100 , 99999);
                if (MillerRabinTest(randomValue, 100))
                    return randomValue;
            }
        }

        public static int GetRandomValue(int minValue = 0, int maxValue = 1)
        {
            Random random = new Random(Guid.NewGuid().GetHashCode());
            return random.Next(minValue, maxValue);
        }

        /// <summary>
        /// Тест Миллера-Рабина для простого числа.
        /// </summary>
        /// <param name="n"></param>
        /// <param name="k"></param>
        /// <returns></returns>
        public static bool MillerRabinTest(BigInteger n, int k)
        {
            // если n == 2 или n == 3 - эти числа простые, возвращаем true
            if (n == 2 || n == 3)
                return true;

            // если n < 2 или n четное - возвращаем false
            if (n < 2 || n % 2 == 0)
                return false;

            // представим n − 1 в виде (2^s)·t, где t нечётно, это можно сделать последовательным делением n - 1 на 2
            BigInteger t = n - 1;

            int s = 0;

            while (t % 2 == 0)
            {
                t /= 2;
                s += 1;
            }

            // повторить k раз
            for (int i = 0; i < k; i++)
            {
                // выберем случайное целое число a в отрезке [2, n − 2]
                RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();

                byte[] _a = new byte[n.ToByteArray().LongLength];

                BigInteger a;

                do
                {
                    rng.GetBytes(_a);
                    a = new BigInteger(_a);
                }
                while (a < 2 || a >= n - 2);

                // x ← a^t mod n, вычислим с помощью возведения в степень по модулю
                BigInteger x = BigInteger.ModPow(a, t, n);

                // если x == 1 или x == n − 1, то перейти на следующую итерацию цикла
                if (x == 1 || x == n - 1)
                    continue;

                // повторить s − 1 раз
                for (int r = 1; r < s; r++)
                {
                    // x ← x^2 mod n
                    x = BigInteger.ModPow(x, 2, n);

                    // если x == 1, то вернуть "составное"
                    if (x == 1)
                        return false;

                    // если x == n − 1, то перейти на следующую итерацию внешнего цикла
                    if (x == n - 1)
                        break;
                }

                if (x != n - 1)
                    return false;
            }

            // вернуть "вероятно простое"
            return true;
        }


        /// <summary>
        /// Реализация расширенного бинарного алгоритма Евклида.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static long[] GCD(long a, long b)
        {
            if (a == 0) return new long[] { b };
            if (b == 0) return new long[] { a };

            long A, B, C, D, u, v, g, x, y;
            int k = 1;

            while (((a | b) & 1) == 0)
            {
                a >>= 1;
                b >>= 1;
                k <<= 1;
            }

            u = a; v = b; A = 1; B = 0; C = 0; D = 1;

            while (u != 0)
            {
                while ((u & 1) == 0)
                {
                    u >>= 1;

                    if (((A | B) & 1) == 0)
                    {
                        A >>= 1;
                        B >>= 1;
                    }
                    else
                    {
                        A = (A + b) / 2;
                        B = (B - a) / 2;
                    }
                }

                while ((v & 1) == 0)
                {
                    v >>= 1;

                    if (((C | D) & 1) == 0)
                    {
                        C >>= 1;
                        D >>= 1;
                    }
                    else
                    {
                        C = (C + b) / 2;
                        D = (D - a) / 2;
                    }
                }

                if (u >= (0 + v))
                {
                    u = u - v;
                    A = A - C;
                    B = B - D;

                }
                else
                {
                    v = v - u;
                    C = C - A;
                    D = D - B;
                }
            }

            g = k * v;
            x = C;
            y = D;

            return new long[] { g, C, D};

        }

        // Алгоритм быстрого возведения числа x в степень y по модулю z
        public static long FEA(long x, long y, long z)
        {
            long c = 1;

            while (y != 0)
            {
                if (y % 2 == 0)
                {
                    y = y / 2;
                    x = (x * x) % z;
                }
                else
                {
                    y -= 1;
                    c = (c * x) % z;
                }
            }

            return c;
        }
    }
}

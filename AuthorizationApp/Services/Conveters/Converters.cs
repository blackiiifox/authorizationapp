﻿using AuthorizationApp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using static AuthorizationApp.Services.Binary;

namespace AuthorizationApp.Services
{

    /// <summary>
    /// Класс конвертера bool в Visibility
    /// </summary>
    public class BooleanToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string parameterString = parameter as string;

            Visibility trueValue = Visibility.Visible;
            Visibility falseValue = Visibility.Hidden;

            string[] parameters;
            if (!string.IsNullOrEmpty(parameterString))
            {
                parameters = parameterString.Split(new char[] { '|' });
                // Now do something with the parameters
                if (parameters.Contains("Inversion"))
                {
                    trueValue = Visibility.Collapsed;
                    falseValue = Visibility.Visible;
                }

                if (parameters.Contains("Collapsed"))
                {
                    if (parameters.Contains("Inversion"))
                    {
                        trueValue = Visibility.Collapsed;
                    }
                    else
                    {
                        falseValue = Visibility.Collapsed;
                    }
                }

            }

            if ((bool)value)
            {
                return trueValue;
            }
            else
            {
                return falseValue;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }


    /// <summary>
    /// Класс конвертера групп в Visble.
    /// </summary>
    public class GroupsToVisibleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            GroupModel Group = value as GroupModel;
            string parameterString = parameter as string;

            Visibility trueValue = Visibility.Visible;
            Visibility falseValue = Visibility.Collapsed;

            string[] parameters;
            if (!string.IsNullOrEmpty(parameterString))
            {
                parameters = parameterString.Split(new char[] { '|' });
                // Now do something with the parameters
                if(parameters.Contains("Inversion"))
                {
                    trueValue = Visibility.Collapsed;
                    falseValue = Visibility.Visible;
                }

                if (parameters.Contains("Hidden"))
                {
                    if (parameters.Contains("Inversion"))
                    {
                        trueValue = Visibility.Hidden;
                    }
                    else
                    {
                        falseValue = Visibility.Hidden;
                    }
                }

            }

            if (Group.GroupPermission.Equals(PermissionType.High))
            {
                return trueValue;
            }
            else
            {
                return falseValue;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Класс конвертера id групп в текстовое представление.
    /// </summary>
    public class GroupsToTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            ObservableCollection<GroupType> IdGroupsList = value as ObservableCollection<GroupType>;
            string outString = "";

            if(IdGroupsList.Count == 0)
            {
                outString = "Нет групп";
            }
            
            foreach(GroupType IdGroup in IdGroupsList)
            {
                outString += GetDescription(IdGroup); ;

                if (!(IdGroupsList.IndexOf(IdGroup) == IdGroupsList.Count - 1))
                {
                    outString += ", ";
                }
            }
            return outString;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public static string GetDescription(Enum value)
        {
            return
                value
                    .GetType()
                    .GetMember(value.ToString())
                    .FirstOrDefault()
                    ?.GetCustomAttribute<DescriptionAttribute>()
                    ?.Description;
        }
    }


    public class EnumDescriptionConverter : IValueConverter
    {
        private string GetEnumDescription(Enum enumObj)
        {
            FieldInfo fieldInfo = enumObj.GetType().GetField(enumObj.ToString());

            object[] attribArray = fieldInfo.GetCustomAttributes(false);

            if (attribArray.Length == 0)
            {
                return enumObj.ToString();
            }
            else
            {
                DescriptionAttribute attrib = attribArray[0] as DescriptionAttribute;
                return attrib.Description;
            }
        }

        object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Enum myEnum = (Enum)value;
            string description = GetEnumDescription(myEnum);
            return description;
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return string.Empty;
        }
    }

    class ObjectToByteConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Writer writer = new Writer();

            List<object> arrayObjects = new List<object>();

            foreach (PropertyInfo a in value.GetType().GetProperties(BindingFlags.Public |
                BindingFlags.Instance | BindingFlags.NonPublic))
            {
                var valueProperty = a.GetValue(value);
                arrayObjects.Add(valueProperty);
            }

            byte[] data = writer.GetRawParams(arrayObjects.ToArray());

            return data;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Reader reader = new Reader((byte[])value);

            foreach (PropertyInfo a in parameter.GetType().GetProperties(BindingFlags.Public |
                BindingFlags.Instance | BindingFlags.NonPublic))
            {
                if (a.PropertyType.Equals(typeof(ushort)))
                    a.SetValue(parameter, reader.ReadUShort());

                if (a.PropertyType.Equals(typeof(int)))
                    a.SetValue(parameter, reader.ReadInt());

                if (a.PropertyType.Equals(typeof(string)))
                    a.SetValue(parameter, reader.ReadString());

                if (a.PropertyType.Equals(typeof(byte)))
                    a.SetValue(parameter, reader.ReadByte());

                if (a.PropertyType.Equals(typeof(float)))
                    a.SetValue(parameter, reader.ReadFloat());

                if (a.PropertyType.Equals(typeof(double)))
                    a.SetValue(parameter, reader.ReadDouble());

                if (a.PropertyType.Equals(typeof(DateTime)))
                    a.SetValue(parameter, reader.ReadDateTime());
            }

            return value;
        }
    }

}

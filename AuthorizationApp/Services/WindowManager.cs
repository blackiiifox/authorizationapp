﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AuthorizationApp.Views;
using System.Windows;
using AuthorizationApp.ViewModels;
// using AuthorizationApp.Models;
using System.ComponentModel;
using MaterialDesignThemes;
using MaterialDesignThemes.Wpf;
using System.Windows.Controls;
using AuthorizationApp.CustomControls;
using System.Collections.ObjectModel;
using AuthorizationApp.Models;

namespace AuthorizationApp.Services
{
    public static class WindowManager
    {
        private static AccessWindow accessWin;
        private static MainWindow mainWin;
        private static PasswordChangerView passWin;
        private static KeyConfigChangerView keyWin;

        private static Window activeWindow;
        private static ViewModelBase activeViewModel;
        private static MainWindowViewModel mainWinVM;
        private static PasswordChangerViewModel passVM;
        private static KeyConfigChangerViewModel keyVM;

        private delegate void CloseWindow();

        public static bool IsDialogWindowShowing = false;

        public static void SetAccessWin(AccessWindow win)
        {
            accessWin = win;
            activeWindow = accessWin;
        }

        public static void GetMainWin(AccountModel account)
        {
            mainWin = new MainWindow();
            mainWinVM = new MainWindowViewModel(account);
            NewWindowInstatiate(mainWin, mainWinVM);
            accessWin.Close();
            // mainWinVM.CheckPassword();
        }

        public static void ShowPasswordChanger(AccountModel account = null)
        {
            passWin = new PasswordChangerView();
            passVM = new PasswordChangerViewModel(account);
            NewWindowInstatiate(passWin, passVM, true);
        }

        public static void ClosePasswordChanger()
        {
            activeWindow.Close();
            activeWindow = mainWin;
            activeViewModel = mainWinVM;
        }

        public static void ShowKeyConfigChanger(AccountModel account)
        {
            keyWin = new KeyConfigChangerView();
            keyVM = new KeyConfigChangerViewModel(account);
            NewWindowInstatiate(keyWin, keyVM, true);
        }

        public static void CloseKeyConfigChanger()
        {
            activeWindow.Close();
            activeWindow = mainWin;
            activeViewModel = mainWinVM;
        }

        public async static Task<bool> ShowSecurityWindow()
        {
            var view = new SecurityWindow
            {
                DataContext = new SecurityWindowViewModel()
            };
            IsDialogWindowShowing = true;
            var result = await DialogHost.Show(view);
            IsDialogWindowShowing = false;
            return (bool)result;
        }

        public async static Task<bool> ShowDictionaryExtender()
        {
            var view = new DictionaryExtenderView
            {
                DataContext = new DictionaryExtenderViewModel()
            };
            IsDialogWindowShowing = true;
            var result = await DialogHost.Show(view);
            IsDialogWindowShowing = false;
            return (bool)result;
        }

        public async static Task<bool> ShowMessagebox(string message, bool isBaseMessage = true)
        {
            string m = isBaseMessage is true? (string)Application.Current.Resources[message] : message;
            var view = new MaterialMessageBox
            {
                DataContext = new MaterialMessageBoxViewModel(m)
            };
            IsDialogWindowShowing = true;
            var result = await DialogHost.Show(view, (activeWindow.Content as DialogHost).Identifier);
            IsDialogWindowShowing = false;
            return (bool)result;
        }

        private static void NewWindowInstatiate(Window win, ViewModelBase vm, bool showAsDialog = false)
        {
            activeViewModel = vm;
            activeWindow = win;
            activeWindow.DataContext = vm;
            if (showAsDialog)
            {
                activeWindow.ShowDialog();
                return;
            }
            activeWindow.Show();
        }
    }
}

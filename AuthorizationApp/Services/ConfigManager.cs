﻿using AuthorizationApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthorizationApp.Services
{
    public class ConfigManager : INotifyPropertyChanged
    {
        #region Variables
        public event PropertyChangedEventHandler PropertyChanged;
        private static ConfigManager instance;
        private static readonly object syncRoot = new object();

        public EncryptionConfigModel EncryptionConfig { get; set; }
        #endregion

        #region Constructors
        private ConfigManager()
        {
            EncryptionConfig = EncryptionConfigModel.Load();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Метод для получения объекта менеджера данных 
        /// </summary>
        /// <returns></returns>
        public static ConfigManager GetInstance()
        {
            if (instance == null)
            {
                lock (syncRoot)
                {
                    if (instance == null)
                        instance = new ConfigManager();
                }
            }
            return instance;
        }

        /// <summary>
        /// Создать новые ключи шифрования паролей
        /// </summary>
        /// <returns></returns>
        public void CreateNewEncryptionPasswordsKey()
        {
            EncryptionConfig.HistoryEncryptionPasswordConfig.Add(EncryptionPasswordConfigModel.GetDefault());
            EncryptionConfigModel.Save(EncryptionConfig);
        }

        public EncryptionPasswordConfigModel GetPasswordEncryptionConfig(DateTime time)
        {
            if (!EncryptionConfig.HistoryEncryptionPasswordConfig.Any())
            {
                EncryptionConfig = EncryptionConfigModel.Load();
            }
            return EncryptionConfig.HistoryEncryptionPasswordConfig.LastOrDefault(x => x.TimeCreate <= time + new TimeSpan(0,0,0,0,50));
        }
        #endregion
    }
}

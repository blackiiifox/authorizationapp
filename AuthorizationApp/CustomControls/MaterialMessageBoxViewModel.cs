﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AuthorizationApp.ViewModels;

namespace AuthorizationApp.CustomControls
{
    /// <summary>
    /// Модель представления диалоговых окон.
    /// </summary>
    public class MaterialMessageBoxViewModel : ViewModelBase
    {
        /// <summary>
        /// Сообщение выводимое в диалоговом окне.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Конструктор для создания модели представления диалогового окна.
        /// </summary>
        /// <param name="message">Сообщение для вывода в диалоговом окне.</param>
        public MaterialMessageBoxViewModel(string message)
        {
            Message = message;
        }
    }
}

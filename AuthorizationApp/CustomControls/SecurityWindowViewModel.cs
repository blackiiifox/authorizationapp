﻿using AuthorizationApp.Models;
using AuthorizationApp.Services;
using AuthorizationApp.ViewModels;
using MaterialDesignThemes.Wpf;
using System;

namespace AuthorizationApp.CustomControls
{
    public class SecurityWindowViewModel : ViewModelBase
    {
        #region Variables
        public ConfigPasswordModel Config { get; set; }

        public TimeSpanView MinTimeSpanView { get; set; }
        public TimeSpanView MaxTimeSpanView { get; set; }
        #endregion


        #region Commads
        private IDelegateCommand saveConfigCommand;
        public IDelegateCommand SaveConfigCommand
        {
            get
            {
                saveConfigCommand = new DelegateCommand(SaveConfig);
                return saveConfigCommand;
            }
        }
        #endregion

        #region Constructors
        public SecurityWindowViewModel()
        {
            Config = DataManager.GetInstance().ConfigPassord.Clone() as ConfigPasswordModel;
            
            MinTimeSpanView = new TimeSpanView(Config.MinLimiter.TimeLimit);
            MaxTimeSpanView = new TimeSpanView(Config.MaxLimiter.TimeLimit);
        }
        #endregion


        #region Methods
        private void SaveConfig(object obj)
        {
            Config.MaxLimiter.TimeLimit = new TimeSpan(MaxTimeSpanView.Days, MaxTimeSpanView.Hours, MaxTimeSpanView.Minutes, 0);
            Config.MinLimiter.TimeLimit = new TimeSpan(MinTimeSpanView.Days, MinTimeSpanView.Hours, MinTimeSpanView.Minutes, 0);

            if (Config.MinLimiter.TimeLimit >= Config.MaxLimiter.TimeLimit)
            {
                System.Windows.MessageBox.Show("Максимальное время жизни пароля должно быть больше минимального");
                return;
            }

            ConfigPasswordModel.SaveConfigPassword(Config);
            DataManager.GetInstance().ConfigPassord = Config;
            DialogHost.CloseDialogCommand.Execute(null, null);
        }
        #endregion
    }

    public class TimeSpanView
    {
        public int Days { get; set; }
        public int Hours { get; set; }
        public int Minutes { get; set; }

        public TimeSpanView(TimeSpan span)
        {
            Days = span.Days;
            Hours = span.Hours;
            Minutes = span.Minutes;
        }
    }

}

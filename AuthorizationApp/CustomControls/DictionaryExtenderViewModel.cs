﻿using AuthorizationApp.ViewModels;
using AuthorizationApp.Services;
using AuthorizationApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MaterialDesignThemes.Wpf;

namespace AuthorizationApp.CustomControls
{
    class DictionaryExtenderViewModel : ViewModelBase
    {
        #region Variablse
        public DictionaryPasswordModel DictionaryPassword { get; set; }
        public DictionaryLoginModel DictionaryLogin { get; set; }

        public string BlockLogin { get; set; } = "";

        public string BlockPassword { get; set; } = "";
        #endregion

        #region Commads
        private IDelegateCommand saveDictionaryCommand;
        public IDelegateCommand SaveDictionaryCommand
        {
            get
            {
                saveDictionaryCommand = new DelegateCommand(SaveDictionary);
                return saveDictionaryCommand;
            }
        }

        private IDelegateCommand addBlockLoginCommand;
        public IDelegateCommand AddBlockLoginCommand
        {
            get
            {
                addBlockLoginCommand = new DelegateCommand(AddBlockLogin);
                return addBlockLoginCommand;
            }
        }

        private IDelegateCommand addBlockPasswordCommand;
        public IDelegateCommand AddBlockPasswordCommand
        {
            get
            {
                addBlockPasswordCommand = new DelegateCommand(AddBlockPassword);
                return addBlockPasswordCommand;
            }
        }
        #endregion

        #region Constructors
        public DictionaryExtenderViewModel()
        {
            DictionaryPassword = DataManager.GetInstance().DictionaryPassword.Clone() as DictionaryPasswordModel;
            DictionaryLogin = DataManager.GetInstance().DictionaryLogin.Clone() as DictionaryLoginModel;
        }
        #endregion


        #region Methods
        private void SaveDictionary(object obj)
        {
            DictionaryPasswordModel.SaveDictionary(DictionaryPassword);
            DictionaryLoginModel.SaveDictionary(DictionaryLogin);

            DataManager.GetInstance().DictionaryPassword = DictionaryPassword;
            DataManager.GetInstance().DictionaryLogin = DictionaryLogin;

            DialogHost.CloseDialogCommand.Execute(null, null);
        }

        private void AddBlockLogin(object obj)
        {
            if(BlockLogin == "")
            {
                System.Windows.MessageBox.Show("Блокируемый логин не может быть пустым");
                return;
            }

            foreach(string log in DictionaryLogin.BlockedLogins)
            {
                if(log == BlockLogin)
                {
                    System.Windows.MessageBox.Show("Данный логин уже заблокирован");
                    return;
                }
            }
            DictionaryLogin.BlockedLogins.Add(BlockLogin);
            System.Windows.MessageBox.Show("Логин \"" + BlockLogin + "\" добавлен в словарь блокировки. Сохраните изменения.");
            BlockLogin = "";
        }

        private void AddBlockPassword(object obj)
        {
            if (BlockPassword == "")
            {
                System.Windows.MessageBox.Show("Блокируемый пароль не может быть пустым");
                return;
            }

            foreach (string pass in DictionaryPassword.BlockedPasswords)
            {
                if (pass == BlockPassword)
                {
                    System.Windows.MessageBox.Show("Данный пароль уже заблокирован");
                    return;
                }
            }
            DictionaryPassword.BlockedPasswords.Add(BlockPassword);
            System.Windows.MessageBox.Show("Пароль \"" + BlockPassword + "\" добавлен в словарь блокировки. Сохраните изменения.");
            BlockPassword = "";
        }
        #endregion

    }
}

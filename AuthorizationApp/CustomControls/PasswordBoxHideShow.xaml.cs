﻿using AuthorizationApp.Services;
using AuthorizationApp.ViewModels;
using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace AuthorizationApp.CustomControls
{
    /// <summary>
    /// User Control PasswordBox, способен отображать скрытый пароль и показывать предупреждения
    /// </summary>
    public partial class PasswordBoxHideShow : UserControl
    {
        // Проблемы: События мыши не биндятся, фокусировка не биндится, свойства контрола не биндятся к внешним объектам

        #region Variables        
        public static readonly DependencyProperty PasswordProperty =
        DependencyProperty.Register(
            "Password",
            typeof(string),
            typeof(PasswordBoxHideShow),
            new FrameworkPropertyMetadata(
                default(PasswordBoxHideShow),
                FrameworkPropertyMetadataOptions.BindsTwoWayByDefault | FrameworkPropertyMetadataOptions.Journal,
                new PropertyChangedCallback(PasswordChange),
                new CoerceValueCallback(PasswordCoerce)
                )
            );

        public static readonly DependencyProperty ControlVisibleProperty =
        DependencyProperty.Register(
            "ControlVisible",
            typeof(Visibility),
            typeof(PasswordBoxHideShow),
            new FrameworkPropertyMetadata(
                default(Visibility),
                FrameworkPropertyMetadataOptions.BindsTwoWayByDefault | FrameworkPropertyMetadataOptions.Journal,
                new PropertyChangedCallback(ControlVisibleChange),
                new CoerceValueCallback(ControlVisibleCoerce)
                )
            );

        public static readonly DependencyProperty WarningMessageProperty =
        DependencyProperty.Register("WarningMessage",
            typeof(string),
            typeof(PasswordBoxHideShow),
            new FrameworkPropertyMetadata(
                default(PasswordBoxHideShow),
                FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                new PropertyChangedCallback(WarningChange),
                new CoerceValueCallback(WarningCoerce)
                )
            );

        
        public static readonly DependencyProperty HintProperty =
        DependencyProperty.Register(
            "Hint",
            typeof(string),
            typeof(PasswordBoxHideShow),
            new FrameworkPropertyMetadata(
                default(PasswordBoxHideShow),
                FrameworkPropertyMetadataOptions.BindsTwoWayByDefault | FrameworkPropertyMetadataOptions.Journal,
                new PropertyChangedCallback(HintChange),
                new CoerceValueCallback(HintCoerce)
                )
            );

        public static readonly DependencyProperty PasswordVisibleProperty =
        DependencyProperty.Register(
            "PasswordVisible",
            typeof(Visibility),
            typeof(PasswordBoxHideShow),
            new FrameworkPropertyMetadata(
                default(Visibility),
                FrameworkPropertyMetadataOptions.BindsTwoWayByDefault | FrameworkPropertyMetadataOptions.Journal,
                new PropertyChangedCallback(PasswordVisibleChange),
                new CoerceValueCallback(PasswordVisibleCoerce)
                )
            );

        public Visibility PasswordVisible
        {
            get { return (Visibility)GetValue(PasswordVisibleProperty); }
            set { SetValue(PasswordVisibleProperty, value); } 
        }

        public string WarningMessage
        {
            get { return (string)GetValue(WarningMessageProperty); }
            set
            {
                SetValue(WarningMessageProperty, value);
                SetWarning(value);
            }
        }

        public Visibility ControlVisible
        {
            get { return (Visibility)GetValue(ControlVisibleProperty); }
            set { SetValue(ControlVisibleProperty, value); }
        }

        public string Hint
        {
            get { return (string)GetValue(HintProperty); }
            set { SetValue(HintProperty, value); }
        }

        public string Password
        {
            get { return (string)GetValue(PasswordProperty); }
            set { SetValue(PasswordProperty, value); }
        }
        #endregion

        #region Constructors
        public PasswordBoxHideShow()
        {
            InitializeComponent();
            (this.Content as FrameworkElement).DataContext = this;
            WarningMessage = "";
            PasswordVisible = Visibility.Hidden;
        }
        #endregion

        #region Events
        private void TxtPasswordbox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            Password = txtPasswordbox.Password;
        }

        private void ImgShowHide_Click(object sender, RoutedEventArgs e)
        {
            if ((bool)ImgShowHide.IsChecked)
            {
                PasswordVisible = Visibility.Visible;
            }
            else
            {
                PasswordVisible = Visibility.Hidden;
            }
        }

        private void TxtVisiblePasswordbox_TextChanged(object sender, TextChangedEventArgs e)
        {
            Password = txtVisiblePasswordbox.Text;
        }
        #endregion

        #region Functions


        private static object PasswordVisibleCoerce(DependencyObject d, object baseValue)
        {
            PasswordBoxHideShow passwordBox = (PasswordBoxHideShow)d;
            if (baseValue == null)
            {
                return Visibility.Hidden;
            }
            return baseValue;
        }

        private static void PasswordVisibleChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            PasswordBoxHideShow passwordBox = (PasswordBoxHideShow)d;
            if (e == null)
            {
                passwordBox.PasswordVisible = Visibility.Hidden;
            }
            else
            {
                passwordBox.PasswordVisible = (Visibility)e.NewValue;
            }

            if (passwordBox.PasswordVisible == Visibility.Visible)
            {
                passwordBox.ShowPassword();
            }
            else
            {
                passwordBox.HidePassword();
            }
        }

        private static void ControlVisibleChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            PasswordBoxHideShow passwordBox = (PasswordBoxHideShow)d;
            if (e == null)
            {
                passwordBox.ControlVisible = Visibility.Visible;
            }
            else
            {
                passwordBox.ControlVisible = (Visibility)e.NewValue;
            }

            passwordBox.ImgShowHide.Visibility = passwordBox.ControlVisible;
            passwordBox.WarningMessage = "";
        }

        private static object ControlVisibleCoerce(DependencyObject d, object baseValue)
        {
            PasswordBoxHideShow passwordBox = (PasswordBoxHideShow)d;
            if (baseValue == null)
            {
                return Visibility.Visible;
            }
            return baseValue;
        }

        private static void HintChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            PasswordBoxHideShow passwordBox = (PasswordBoxHideShow)d;
            if (e == null)
            {
                passwordBox.Hint = "";
            }
            else
            {
                passwordBox.Hint = (string)e.NewValue;
            }
        }

        private static object HintCoerce(DependencyObject d, object baseValue)
        {
            PasswordBoxHideShow passwordBox = (PasswordBoxHideShow)d;
            if (baseValue == null)
            {
                return "";
            }
            return baseValue;
        }

        private static void PasswordChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            PasswordBoxHideShow passwordBox = (PasswordBoxHideShow)d;
            if (e == null)
            {
                passwordBox.Password = "";
            }
            else
            {
                passwordBox.Password = (string)e.NewValue;

                if(passwordBox.Password == "")
                {
                    passwordBox.txtPasswordbox.Password = "";
                    passwordBox.txtVisiblePasswordbox.Text = "";
                }
            }
        }

        private static object PasswordCoerce(DependencyObject d, object baseValue)
        {
            PasswordBoxHideShow passwordBox = (PasswordBoxHideShow)d;
            if (baseValue == null)
            {
                return "";
            }
            return baseValue;
        }

        private static object WarningCoerce(DependencyObject d, object baseValue)
        {
            PasswordBoxHideShow passwordBox = (PasswordBoxHideShow)d;
            if (baseValue == null)
            {
                return "";
            }
            return baseValue;
        }

        private static void WarningChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            PasswordBoxHideShow passwordBox = (PasswordBoxHideShow)d;
            if (e == null)
            {
                passwordBox.WarningMessage = "";
            }
            else
            {
                passwordBox.WarningMessage = (string)e.NewValue;
            }
        }

        private void SetWarning(string newWarning)
        {
            if (ControlVisible == Visibility.Visible)
            {
                if (newWarning.Count() > 0)
                    ImgError.Visibility = Visibility.Visible;
                else
                    ImgError.Visibility = Visibility.Hidden;
                ImgError.ToolTip = newWarning;
            }
            else
            {
                ImgError.Visibility = Visibility.Hidden;
            }
            
        }

        private void HidePassword()
        {
            txtVisiblePasswordbox.Visibility = Visibility.Hidden;
            txtPasswordbox.Visibility = Visibility.Visible;
            txtPasswordbox.Password = Password;
        }

        private void ShowPassword()
        {
            txtVisiblePasswordbox.Visibility = Visibility.Visible;
            txtPasswordbox.Visibility = Visibility.Hidden;
            txtVisiblePasswordbox.Text = Password;
        }
        #endregion
    }
}

﻿using AuthorizationApp.Models;
using AuthorizationApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace AuthorizationApp.ViewModels
{
    class PasswordChangerViewModel : ViewModelBase
    {

        #region Variables
        public AccountModel Account { get; set; }

        public ConfigPasswordModel Config { get; set; } = DataManager.GetInstance().ConfigPassord;
        public bool IsNewAcc { get; set; }

        public string Login { get; set; } = "";
        public string Password { get; set; } = "";
        public string PasswordConfirming { get; set; } = "";

        public bool CloseOn { get; set; } = true;

        /// <summary>
        /// Сообщения для отображения предупреждения password боксов
        /// </summary>
        public string WarningMessage { get; set; }

        private DispatcherTimer timer = new DispatcherTimer();

        public delegate void KeyboardLayoutChanged(int oldCultureInfo, int newCultureInfo);
        public delegate void KeyboardCapsChanged(bool oldCapsState, bool newCapsState);

        public bool IsCapsLocked { get; private set; }
        public bool IsRussianLayout { get; private set; }
        #endregion

        #region Commands
        private IDelegateCommand closeWindowCommand;
        public IDelegateCommand CloseWindowCommand
        {
            get
            {
                closeWindowCommand = new DelegateCommand(CloseWindow);
                return closeWindowCommand;
            }
        }

        private IDelegateCommand acceptHandleCommand;
        public IDelegateCommand AcceptHandleCommand
        {
            get
            {
                acceptHandleCommand = new DelegateCommand(AcceptHandle);
                return acceptHandleCommand;
            }
        }
        #endregion

        #region Constructor
        public PasswordChangerViewModel(AccountModel acc = null)
        {
            if(acc == null)
            {
                Account = AccountModel.GetDefault();
                IsNewAcc = true;
            }
            else
            {
                Account = acc;
                IsNewAcc = false;

                timer.Interval = TimeSpan.FromMilliseconds(200);
                timer.Tick += ShowReasonChange;
                timer.Start();
            }
            if (Config.AutoGenerateEnable)
            {
                Password = PasswordGenerator.GetPassword(8);
            }
            StartCheckTimer();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Вывод причины смены пароля
        /// </summary>
        public void ShowReasonChange(object sender, EventArgs e)
        {
            timer.Stop();
            if (Config.MaxLimiter.TimeLimitEnable)
            {
                // Срок действия пароля истек
                if (Account.Passwords.Last().TimeCreate + Config.MaxLimiter.TimeLimit < DateTime.Now)
                {
                    WindowManager.ShowMessagebox("PasswordExpired");
                    CloseOn = false;
                }
            }

            // Если это новый аккаунт и включена принудительная замена пароля нового аккаунта
            if (Config.ForcedChangeEnable && Account.IsNewAccount)
            {
                WindowManager.ShowMessagebox("FirstEntryAndChangePassword");
                CloseOn = false;
            }


        }

        private void SetWarning()
        {
            string newWarning = "";
            if (IsRussianLayout == true)
            {
                newWarning = "Выбрана русская раскладка";
            }
            if (IsCapsLocked == true)
            {
                if (newWarning.Count() > 0)
                {
                    newWarning += "\n";
                }
                newWarning += "Нажат CapsLock";
            }
            WarningMessage = newWarning;
        }

        private void StartCheckTimer()
        {
            KeyboardWatcher Watcher = new KeyboardWatcher();

            Watcher.KeyboardLayoutChanged += (o, n) =>
            {
                if (o == KeyboardWatcher.IdRussianLayout)
                    IsRussianLayout = true;
                else
                    IsRussianLayout = false;
                SetWarning();
            };

            Watcher.KeyboardCapsChanged += (o, n) =>
            {
                IsCapsLocked = n;
                SetWarning();
            };
        }

        private void CloseWindow(object obj)
        {
            WindowManager.ClosePasswordChanger();
        }

        private void AcceptHandle(object obj)
        {
            if(CheckPasswordField())
            {
                // Если это создание нового аккаунта
                if (IsNewAcc)
                {
                    if (CheckLoginField())
                    {
                        // Выполняем сохранение
                        Account.CurrentPassword = this.Password;
                        Account.Login = this.Login;

                        if (DataManager.GetInstance().CreateNewAccount(Account))
                        {
                            CloseWindow(this);
                            WindowManager.ShowMessagebox("ConfirmCreateAcc");
                        }
                    }
                }

                // Если это смена пароля
                else
                {
                    if(Login != Account.CurrentPassword)
                    {
                        WindowManager.ShowMessagebox("PasswordsNotEquals");
                    }

                    PasswordModel password = new PasswordModel
                    {
                        TimeCreate = DateTime.Now,
                        Password = Password
                    };

                    if(DataManager.GetInstance().PasswordChange(Account, password))
                    {
                        CloseWindow(this);
                        WindowManager.ShowMessagebox("ConfirmPassChange");
                    }
                }
            }
        }


        private bool CheckLoginField()
        {
            if (Login == "")
            {
                WindowManager.ShowMessagebox("NameIsEmpty");
                return false;
            }
            return true;
        }

        private bool CheckPasswordField()
        {
            if (!Config.AutoGenerateEnable)
            {
                if (Password == "" || PasswordConfirming == "")
                {
                    WindowManager.ShowMessagebox("PasswordIsEmpty");
                    return false;
                }


                if (Password.Length < 6)
                {
                    WindowManager.ShowMessagebox("PasswordLengthError");
                    return false;
                }

                if (Password != PasswordConfirming)
                {
                    WindowManager.ShowMessagebox("PasswordsAreDifferent");
                    return false;
                }
            }
            return true;
        }
        #endregion
    }
}

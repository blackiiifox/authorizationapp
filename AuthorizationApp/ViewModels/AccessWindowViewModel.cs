﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using AuthorizationApp.Models;
using AuthorizationApp.Services;

namespace AuthorizationApp.ViewModels
{
    public class AccessWindowViewModel : ViewModelBase
    {
        #region Variables
        private readonly DataManager dataManager = DataManager.GetInstance();
        public ConfigPasswordModel Config { get; set; } = DataManager.GetInstance().ConfigPassord;

        /// <summary>
        /// Выполняется авторизация или регистрация
        /// Если true, то регистрация.
        /// </summary>
        public bool IsAuthOrReg { get; set; }

        /// <summary>
        /// Сообщения для отображения предупреждения password боксов
        /// </summary>
        public string WarningMessage { get; set; }


        /// <summary>
        /// Текст поля логина
        /// </summary>
        public string Login { get; set; } = "";

        /// <summary>
        /// Текст поля пароля
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Текст поля подтверждения пароля.
        /// </summary>
        public string PasswordConfirming { get; set; }

        public delegate void KeyboardLayoutChanged(int oldCultureInfo, int newCultureInfo);
        public delegate void KeyboardCapsChanged(bool oldCapsState, bool newCapsState);

        public bool IsCapsLocked { get; private set; }
        public bool IsRussianLayout { get; private set; }

        private int countTryEnter = 0;
        #endregion

        #region Commands
        private ICommand changeCommand;
        private ICommand acceptCommand;

        /// <summary>
        /// Команда смены режима от ображения.
        /// </summary>
        public ICommand ChangeCommand
        {
            get
            {
                changeCommand = new RelayCommand(param => this.ChangeHandler());
                return changeCommand;
            }
        }

        /// <summary>
        /// Команда подтверждения ввода.
        /// </summary>
        public ICommand AcceptCommand
        {
            get
            {
                acceptCommand = new RelayCommand(param => this.AcceptHandler());
                return acceptCommand;
            }
        }
        #endregion

        #region Constructors
        public AccessWindowViewModel()
        {
            StartCheckTimer();
        }
        #endregion

        #region Methods
        private void AcceptHandler()
        {
            if (!IsAuthOrReg)
            {
                // Если это авторизация, то
                if (CheckAuthAccountInfo())
                {
                    AccountModel account = dataManager.GetAccount(Login);
                    if (account != null)
                    {
                        if (account.CurrentPassword != this.Password)
                        {
                            countTryEnter++;

                            if (countTryEnter == 3)
                            {
                                // WindowManager.ShowMessagebox("AccountPassError");
                                System.Windows.MessageBox.Show("Пароль введен неправильно 3 раза. Приложение закрывается.");
                                System.Windows.Application.Current.Shutdown();
                                return;
                            }

                            WindowManager.ShowMessagebox("AccountPassError");
                            return;
                        }

                        if (account.IsBlocked == true)
                        {
                            WindowManager.ShowMessagebox("AccountBlocked");
                            return;
                        }

                        WindowManager.GetMainWin(account);
                    }
                    else
                    {
                        WindowManager.ShowMessagebox("AccountNotFound");
                    }
                }          
            }
            else
            {
                // Если это регистрация
                if (CheckNewAccountInfo())
                {
                    // Выполняем сохранение

                    AccountModel newAccount = AccountModel.GetDefault();

                    newAccount.CurrentPassword = this.Password;
                    newAccount.Login = this.Login;

                    if(dataManager.CreateNewAccount(newAccount))
                    {
                        WindowManager.ShowMessagebox("ConfirmCreateAcc");
                        ChangeHandler();
                    }
                }

            }
        }

        /// <summary>
        /// Выполняет проверку авторизационных полей. Предварительная проверка визуала
        /// </summary>
        /// <returns>True - данные корректны</returns>
        private bool CheckAuthAccountInfo()
        {
            if (Login == "")
            {
                WindowManager.ShowMessagebox("NameIsEmpty");
                return false;
            }

            if (Password == "")
            {
                WindowManager.ShowMessagebox("PasswordIsEmpty");
                return false;
            }

            return true;
        }

        /// <summary>
        /// Выполняет проверку регистрационных полей. Предварительная проверка визуала
        /// </summary>
        /// <returns>True - данные корректны</returns>
        private bool CheckNewAccountInfo()
        {
            if (Login == "")
            {
                WindowManager.ShowMessagebox("NameIsEmpty");
                return false;
            }

            if (!Config.AutoGenerateEnable)
            {
                if (Password == "" || PasswordConfirming == "")
                {
                    WindowManager.ShowMessagebox("PasswordIsEmpty");
                    return false;
                }

                if(Password.Length < 6)
                {
                    WindowManager.ShowMessagebox("PasswordLengthError");
                    return false;
                }

                if (Password != PasswordConfirming)
                {
                    WindowManager.ShowMessagebox("PasswordsAreDifferent");
                    return false;
                }
            }

            return true;
        }

        private void ChangeHandler()
        {
            IsAuthOrReg = !IsAuthOrReg;

            Login = "";
            Password = "";
            PasswordConfirming = "";

            if (IsAuthOrReg == true) // Если это регистрация
            {
                // Если включаена автоматическая генерация паролей
                if(dataManager.ConfigPassord.AutoGenerateEnable)
                {
                    Password = PasswordGenerator.GetPassword(8);
                }
            }
        }

        private void StartCheckTimer()
        {
            KeyboardWatcher Watcher = new KeyboardWatcher();

            Watcher.KeyboardLayoutChanged += (o, n) =>
            {
                if (o == KeyboardWatcher.IdRussianLayout)
                    IsRussianLayout = true;
                else
                    IsRussianLayout = false;
                SetWarning();
            };

            Watcher.KeyboardCapsChanged += (o, n) =>
            {
                IsCapsLocked = n;
                SetWarning();
            };
        }

        private void SetWarning()
        {
            string newWarning = "";
            if (IsRussianLayout == true)
            {
                newWarning = "Выбрана русская раскладка";
            }
            if (IsCapsLocked == true)
            {
                if (newWarning.Count() > 0)
                {
                    newWarning += "\n";
                }
                newWarning += "Нажат CapsLock";
            }
            WarningMessage = newWarning;
        }
        #endregion
    }
}

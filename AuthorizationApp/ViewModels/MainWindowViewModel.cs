﻿using AuthorizationApp.Models;
using AuthorizationApp.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Threading;

namespace AuthorizationApp.ViewModels
{
    class MainWindowViewModel : ViewModelBase
    {
        #region Variables
        public DataManager Data { get; set; }

        public AccountModel AuthorizedAccount { get; set; }
        public GroupModel AccountGroup
        {
            get
            {
                return Data.GroupsList.FirstOrDefault(group => group.GroupName == AuthorizedAccount.GroupName);
            }
        }
        // Для режима пользователя
        public ObservableCollection<MessageModel> ReceivedMessages { get; set; } = new ObservableCollection<MessageModel>();
        public GroupModel SelectedGroupSend { get; set; }
        public string TextNewMessage { get; set; } = "";

        List<string> oldMessagesPath = new List<string>();
        // readonly DispatcherTimer TimerListner = new DispatcherTimer();

        // Для режима администратора
        public AccountModel SelectedAccount { get; set; }
        public ICollectionView AccountView { get; set; }
        private string searchText = "";

        public string SearchText
        {
            get
            {
                return searchText;
            }
            set
            {
                searchText = value;

                AccountView.Filter = (obj) =>
                {
                    if (obj is AccountModel account)
                    {
                        return account.FindProperty(SearchText.ToLower());
                    }
                    return false;
                };

                AccountView.Refresh();
            }
        }


        private bool aminMenuIsVisible = false;
        public bool AdminMenuIsVisible
        {
            get
            {
                return aminMenuIsVisible;
            }
            set
            {
                aminMenuIsVisible = value;
                if (value == true)
                {
                    UserMenuIsVisible = false;
                }
            }
        }

        private bool userMenuIsVisible = true;
        public bool UserMenuIsVisible
        {
            get
            {
                return userMenuIsVisible;
            }
            set
            {
                userMenuIsVisible = value;
                if (value == true)
                {
                    AdminMenuIsVisible = false;
                }
            }
        }
        #endregion

        #region Commands
        private IDelegateCommand showDictionaryExtenderCommand;
        public IDelegateCommand ShowDictionaryExtenderCommand
        {
            get
            {
                showDictionaryExtenderCommand = new DelegateCommand(ShowDictionaryExtender);
                return showDictionaryExtenderCommand;
            }
        }

        private IDelegateCommand showInfoProgramCommand;
        public IDelegateCommand ShowInfoProgramCommand
        {
            get
            {
                showInfoProgramCommand = new DelegateCommand(ShowInfoProgram);
                return showInfoProgramCommand;
            }
        }

        private IDelegateCommand deleteCommand;
        public IDelegateCommand DeleteCommand
        {
            get
            {
                deleteCommand = new DelegateCommand(DeleteAccaunt);
                return deleteCommand;
            }
        }

        private IDelegateCommand showSecurityWindowCommand;
        public IDelegateCommand ShowSecurityWindowCommand
        {
            get
            {
                showSecurityWindowCommand = new DelegateCommand(ShowSecurityWindow);
                return showSecurityWindowCommand;
            }
        }


        private IDelegateCommand saveCommand;
        public IDelegateCommand SaveCommand
        {
            get
            {
                saveCommand = new DelegateCommand(SaveAccount);
                return saveCommand;
            }
        }


        private IDelegateCommand resetPassCommand;
        public IDelegateCommand ResetPassCommand
        {
            get
            {
                resetPassCommand = new DelegateCommand(ResetPass);
                return resetPassCommand;
            }
        }

        private IDelegateCommand changeModAdminCommand;
        public IDelegateCommand ChangeModAdminCommand
        {
            get
            {
                changeModAdminCommand = new DelegateCommand(ChangeModAdmin);
                return changeModAdminCommand;
            }
        }


        private IDelegateCommand changeModUserCommand;
        public IDelegateCommand ChangeModUserCommand
        {
            get
            {
                changeModUserCommand = new DelegateCommand(ChangeModUser);
                return changeModUserCommand;
            }
        }

        private IDelegateCommand newAccountCommand;
        public IDelegateCommand NewAccountCommand
        {
            get
            {
                newAccountCommand = new DelegateCommand(NewAccount);
                return newAccountCommand;
            }
        }

        private IDelegateCommand changePasswordCommand;
        public IDelegateCommand ChangePasswordCommand
        {
            get
            {
                changePasswordCommand = new DelegateCommand(ChangePassword);
                return changePasswordCommand;
            }
        }


        private IDelegateCommand keyConfigurationChangeCommand;
        public IDelegateCommand KeyConfigurationChangeCommand
        {
            get
            {
                keyConfigurationChangeCommand = new DelegateCommand(KeyConfigurationChange);
                return keyConfigurationChangeCommand;
            }
        }

        private IDelegateCommand sendMessageToAccountGroupCommand;
        public IDelegateCommand SendMessageToAccountGroupCommand
        {
            get
            {
                sendMessageToAccountGroupCommand = new DelegateCommand(SendMessageToAccountGroup);
                return sendMessageToAccountGroupCommand;
            }
        }

        private IDelegateCommand sendMessageToSelectedGroupCommand;
        public IDelegateCommand SendMessageToSelectedGroupCommand
        {
            get
            {
                sendMessageToSelectedGroupCommand = new DelegateCommand(SendMessageToSelectedGroup);
                return sendMessageToSelectedGroupCommand;
            }
        }

        private IDelegateCommand sendMessageToAllGroupsCommand;
        public IDelegateCommand SendMessageToAllGroupsCommand
        {
            get
            {
                sendMessageToAllGroupsCommand = new DelegateCommand(SendMessageToAllGroups);
                return sendMessageToAllGroupsCommand;
            }
        }
        #endregion

        #region Constructor
        public MainWindowViewModel(AccountModel account)
        {
            AuthorizedAccount = account;

            Data = DataManager.GetInstance();

            if (Data.GroupsList.FirstOrDefault(group => group.GroupName.Equals(AuthorizedAccount.GroupName)).GroupPermission.Equals(PermissionType.High))
            {
                AccountView = CollectionViewSource.GetDefaultView(Data.AccountsList);
                AdminMenuIsVisible = true;
            }
            CheckPassword();

            ListenMessages();
            ///asdasdasdsad
            //TimerListner.Tick += new EventHandler(ListenMessages);
            //TimerListner.Interval = new TimeSpan(0, 0, 1);
            //TimerListner.Start();
        }
        #endregion

        #region Methods
        private void ListenMessages()
        {
            List<string> messages = PathManager.GetMessagesFilesPath(AuthorizedAccount.ID.ToString());

            if (messages.Count > oldMessagesPath.Count())
            {
                // Выполяем получение нового сообщения
                List<string> newMessagesPath = messages.Except(oldMessagesPath).Union(oldMessagesPath.Except(messages)).ToList();
                // List<string> newMessagesPath = messages.Where(message => oldMessagesPath.Where(oldMessage => oldMessage == message).Any()).ToList();
                for (int i = 0; i < newMessagesPath.Count(); i++)
                {
                    string path = newMessagesPath[i];
                    long length = new System.IO.FileInfo(path).Length;
                    if (length > 0)
                    {
                        try
                        {
                            MessageModel message = MessageModel.Load(path);

                            KeyMessageEncryptionModel keyEncryption =
                                AuthorizedAccount.ListKeysMessagesEncryption.LastOrDefault(keyElem => keyElem.TimeCreate <= message.TimeSend);

                            message.Message = keyEncryption != null ? MessageEncryption.Decryption(message.EncryptionMessage, keyEncryption) : "Ключ расшифровки не найден";

                            ReceivedMessages.Add(message);
                        }
                        catch
                        {
                            newMessagesPath.RemoveAt(i);
                            i -= 1;
                        }
                    }
                }

                ReceivedMessages = new ObservableCollection<MessageModel>(ReceivedMessages.OrderBy(x => x.TimeSend));
                // Устанавливаем полученные пути в список
                oldMessagesPath.AddRange(newMessagesPath);
            }
            else
            {
                if (messages.Count < oldMessagesPath.Count())
                {
                    ReceivedMessages.Clear();
                    oldMessagesPath.Clear();
                }
            }
        }

        private void DeleteAccaunt(object param)
        {
            Data.DeleteAccount(SelectedAccount);
        }

        private void SaveAccount(object obj)
        {
            AccountModel.SaveAccount(SelectedAccount);
        }

        private void ResetPass(object obj)
        {
            if (SelectedAccount.CurrentPassword == "Qwerty1")
            {
                WindowManager.ShowMessagebox((string)Application.Current.Resources["PasswordChangeExistError"] + " " + "Qwerty1" + ".", false);
                return;
            }

            SelectedAccount.CurrentPassword = "Qwerty1";
            AccountModel.SaveAccount(SelectedAccount);
            WindowManager.ShowMessagebox("PasswordReset");
        }

        private void ChangeModAdmin(object obj)
        {
            AdminMenuIsVisible = true;
        }

        private void ChangeModUser(object obj)
        {
            UserMenuIsVisible = true;
        }

        private void ShowSecurityWindow(object obj)
        {
            WindowManager.ShowSecurityWindow();
        }

        private void ShowInfoProgram(object obj)
        {
            WindowManager.ShowMessagebox("ProgramInfo");
        }

        private void ShowDictionaryExtender(object obj)
        {
            WindowManager.ShowDictionaryExtender();
        }

        public void CheckPassword()
        {
            if (Data.ConfigPassord.MaxLimiter.TimeLimitEnable)
            {
                // Срок действия пароля истек
                if (AuthorizedAccount.Passwords.Last().TimeCreate + Data.ConfigPassord.MaxLimiter.TimeLimit < DateTime.Now)
                {
                    // WindowManager.ShowMessagebox("PasswordExpired");
                    WindowManager.ShowPasswordChanger(AuthorizedAccount);
                }
            }

            // Если это новый аккаунт и включена принудительная замена пароля нового аккаунта
            if (Data.ConfigPassord.ForcedChangeEnable && AuthorizedAccount.IsNewAccount)
            {
                // WindowManager.ShowMessagebox("SuchAccountExists");
                WindowManager.ShowPasswordChanger(AuthorizedAccount);
            }
            else
            {
                if (AuthorizedAccount.IsNewAccount)
                {
                    AuthorizedAccount.IsNewAccount = false;
                    AccountModel.SaveAccount(AuthorizedAccount);
                }
            }
        }

        private void NewAccount(object obj)
        {
            WindowManager.ShowPasswordChanger();
        }

        private void ChangePassword(object obj)
        {
            WindowManager.ShowPasswordChanger(AuthorizedAccount);
        }

        private void KeyConfigurationChange(object obj)
        {
            WindowManager.ShowKeyConfigChanger(AuthorizedAccount);
        }

        /// <summary>
        /// Отправка сообщения всем группам
        /// </summary>
        /// <param name="obj"></param>
        private void SendMessageToAllGroups(object obj)
        {
            MessageModel newMessage = new MessageModel()
            {
                TimeSend = DateTime.Now,
                Message = TextNewMessage,
                SenderLogin = AuthorizedAccount.Login
            };

            foreach(var item in Data.GroupsList)
            {
                SendMessageToGrop(item, newMessage);
            }

            ReceivedMessages.Add(newMessage);
            TextNewMessage = "";
        }

        /// <summary>
        /// Отправка сообщения выбранной группе(для режима администратора)
        /// </summary>
        /// <param name="obj"></param>
        private void SendMessageToSelectedGroup(object obj)
        {
            MessageModel newMessage = new MessageModel()
            {
                TimeSend = DateTime.Now,
                Message = TextNewMessage,
                SenderLogin = AuthorizedAccount.Login
            };

            if(SelectedGroupSend is null)
            {
                WindowManager.ShowMessagebox("Выберите группу", false);
                return;
            }

            SendMessageToGrop(SelectedGroupSend, newMessage);

            foreach (var item in Data.GroupsList.Where(groupElem => groupElem.GroupPermission == PermissionType.High && groupElem.GroupName != SelectedGroupSend.GroupName))
            {
                SendMessageToGrop(item, newMessage);
            }

            GroupModel authorizedGroup = Data.GroupsList.FirstOrDefault(groupElem => groupElem.GroupName == AuthorizedAccount.GroupName);

            if (SelectedGroupSend.GroupName == AuthorizedAccount.GroupName || authorizedGroup.GroupPermission == PermissionType.High)
            {
                ReceivedMessages.Add(newMessage);
            }
            
            TextNewMessage = "";
        }


        /// <summary>
        /// Отправка собщения группе аккаунта
        /// </summary>
        /// <param name="obj"></param>
        private void SendMessageToAccountGroup(object obj)
        {
            MessageModel newMessage = new MessageModel()
            {
                TimeSend = DateTime.Now,
                Message = TextNewMessage,
                SenderLogin = AuthorizedAccount.Login
            };

            GroupModel group = Data.GroupsList.FirstOrDefault(groupElem => groupElem.GroupName == AuthorizedAccount.GroupName);

            SendMessageToGrop(group, newMessage);

            foreach (var item in Data.GroupsList.Where(groupElem => groupElem.GroupPermission == PermissionType.High && groupElem.GroupName != group.GroupName))
            {
                SendMessageToGrop(item, newMessage);
            }

            ReceivedMessages.Add(newMessage);
            TextNewMessage = "";

        }

        private void SendMessageToGrop(GroupModel group, MessageModel newMessage)
        {
            if (group == null)
            {
                WindowManager.ShowMessagebox("Группа " + AuthorizedAccount.GroupName + " не найдена.", false);
                return;
            }

            //  && userElem.ID != AuthorizedAccount.ID
            List<AccountModel> accountsList = Data.AccountsList.Where(userElem => userElem.GroupName == group.GroupName).ToList();

            foreach (var account in accountsList)
            {

                KeyMessageInfoModel keyMessageInfo = group.KeyMessageEncryptions.LastOrDefault(key => key.IdAccount == account.ID);

                if (keyMessageInfo != null)
                {
                    // ReceivedMessages.Add(newMessage);
                    string path = PathManager.GetMessageFilePath(account.ID.ToString());
                    // KeyMessageInfoModel keyInfo = group.KeyMessageEncryptions.LastOrDefault(keyInfoElem => keyInfoElem.IdAccount == account.ID);

                    newMessage.EncryptionMessage = MessageEncryption.Encryption(newMessage.Message, keyMessageInfo);

                    MessageModel.Save(newMessage, path);
                }
                else
                {
                    WindowManager.ShowMessagebox("Пользователь " + account.Login + " еще ни разу не отправлял ключ в свою группу.", false);
                }

            }
        }
        #endregion
    }
}

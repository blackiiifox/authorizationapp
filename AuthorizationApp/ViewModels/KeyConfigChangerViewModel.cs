﻿using AuthorizationApp.Models;
using AuthorizationApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace AuthorizationApp.ViewModels
{
    class KeyConfigChangerViewModel : ViewModelBase
    {

        #region Variables
        public AccountModel Account { get; set; }
        public GroupModel AccountGroup { get; set; }

        public int CloseKey { get; set; } = 1231236;
        public int OpenKey { get; set; } = 123123;

        public DataManager Data { get; set; }
        public GroupModel SelectedGroupSend { get; set; }
        public bool IsNewKey { get; set; }

        public int ValueP { get; set; }
        public int ValueQ { get; set; }
        #endregion

        #region Commands
        private IDelegateCommand closeWindowCommand;
        public IDelegateCommand CloseWindowCommand
        {
            get
            {
                closeWindowCommand = new DelegateCommand(CloseWindow);
                return closeWindowCommand;
            }
        }

        private IDelegateCommand acceptHandleCommand;
        public IDelegateCommand AcceptHandleCommand
        {
            get
            {
                acceptHandleCommand = new DelegateCommand(AcceptHandle);
                return acceptHandleCommand;
            }
        }

        private IDelegateCommand sendKeyToSelectedGroupCommand;
        public IDelegateCommand SendKeyToSelectedGroupCommand
        {
            get
            {
                sendKeyToSelectedGroupCommand = new DelegateCommand(SendKeyToSelectedGroup);
                return sendKeyToSelectedGroupCommand;
            }
        }

        private IDelegateCommand sendKeyToAllGroupsCommand;
        public IDelegateCommand SendKeyToAllGroupsCommand
        {
            get
            {
                sendKeyToAllGroupsCommand = new DelegateCommand(SendKeyToAllGroups);
                return sendKeyToAllGroupsCommand;
            }
        }

        private IDelegateCommand sendKeyToAccountGroupCommand;
        public IDelegateCommand SendKeyToAccountGroupCommand
        {
            get
            {
                sendKeyToAccountGroupCommand = new DelegateCommand(SendKeyToAccountGroup);
                return sendKeyToAccountGroupCommand;
            }
        }

        private IDelegateCommand generatePrimeCommand;
        public IDelegateCommand GeneratePrimeCommand
        {
            get
            {
                generatePrimeCommand = new DelegateCommand(GeneratePrime);
                return generatePrimeCommand;
            }
        }
        #endregion

        #region Constructor
        public KeyConfigChangerViewModel(AccountModel acc)
        {
            Data = DataManager.GetInstance();
            Account = acc;
            AccountGroup = Data.GroupsList.FirstOrDefault(group => group.GroupName == Account.GroupName);

            if(Account.CurrentKeyMessagesEncryption == null)
            {
                IsNewKey = true;
            }

        }
        #endregion

        #region Methods
        private void SendKeyToAllGroups(object obj)
        {
            foreach(var group in Data.GroupsList)
            {
                if (!SendKeyMessageEncryption(group))
                {
                    break;
                }
            }
            WindowManager.ShowMessagebox("Ключ отправлен всем группам.", false);
        }

        private void SendKeyToSelectedGroup(object obj)
        {
            SendKeyMessageEncryption(SelectedGroupSend);
            WindowManager.ShowMessagebox("Ключ отправлен группе " + SelectedGroupSend.GroupName + ".", false);
        }

        private void SendKeyToAccountGroup(object obj)
        {
            SendKeyMessageEncryption(Data.GroupsList.FirstOrDefault(group => group.GroupName.Equals(Account.GroupName)));
            SendKeyMessageEncryption(Data.GroupsList.FirstOrDefault(group => group.GroupPermission.Equals(PermissionType.High)));
            WindowManager.ShowMessagebox("Ключ отправлен вашей группе и администратору.", false);
        }

        private bool SendKeyMessageEncryption(GroupModel group)
        {
            if(group == null)
            {
                WindowManager.ShowMessagebox("Группа" + group.GroupName + " не найдена", false);
                return false;
            }

            if (Account.CurrentKeyMessagesEncryption != null)
            {
                KeyMessageInfoModel key = new KeyMessageInfoModel(Account, Account.CurrentKeyMessagesEncryption);

                if(!group.KeyMessageEncryptions.Where(keyMessage => keyMessage.IdAccount == Account.ID && keyMessage.OpenKey == key.OpenKey).Any())
                {
                    group.KeyMessageEncryptions.Add(key);
                    GroupModel.SaveGroup(group);
                }
                return true;
            }
            else
            {
                WindowManager.ShowMessagebox("Ключи шифрования еще не были созданы", false);
                return false;
            }
        }

        private void CloseWindow(object obj)
        {
            WindowManager.ClosePasswordChanger();
        }

        private bool CheckValuePandValueQ()
        {
            bool testP = MessageEncryption.MillerRabinTest(ValueP, 100);
            bool testQ = MessageEncryption.MillerRabinTest(ValueQ, 100);

            if (ValueP == ValueQ)
            {
                WindowManager.ShowMessagebox("Числа 'p' и 'q' не должны быть равны.", false);
                return false;
            }

            if (ValueP <= 99 || ValueP >= 100000 || ValueQ <= 99 || ValueQ >= 100000)
            {
                WindowManager.ShowMessagebox("Число 'p' или 'q' выходит за допустимый диапазон.", false);
                return false;
            }

            if (testP == false && testQ == false)
            {
                WindowManager.ShowMessagebox("Числа 'p' и 'q' не является простыми.", false);
                return false;
            }

            if (testP)
            {
                if (testQ)
                {
                    return true;
                }
                else
                {
                    WindowManager.ShowMessagebox("Число 'p' не является простым.", false);
                    return false;
                }
            }
            else
            {
                WindowManager.ShowMessagebox("Число 'q' не является простым.", false);
                return false;
            }
        }

        private void GeneratePrime(object obj)
        {
            ValueP = MessageEncryption.GetPrime();
            ValueQ = MessageEncryption.GetPrime();
        }

        public delegate KeyMessageEncryptionModel DoIt(int valueQ, int valueP, AutoResetEvent autoResetEvent);

        private static KeyMessageEncryptionModel GenerateNewKeyThread(int valueQ, int valueP, AutoResetEvent autoResetEvent)
        {
            KeyMessageEncryptionModel newKey = KeyMessageEncryptionModel.GenerateNewKey(valueQ, valueP);
            autoResetEvent.Set();
            return newKey;
        }

        private void AcceptHandle(object obj)
        {
            // Если это создание нового ключа
            if (IsNewKey)
            {

                if (CheckValuePandValueQ())
                {
                    // Тут генерация нового ключа

                    DoIt delegateGenerateNewKey = GenerateNewKeyThread;

                    AutoResetEvent autoResetEvent = new AutoResetEvent(false);

                    IAsyncResult r1 = delegateGenerateNewKey.BeginInvoke(ValueQ, ValueP, autoResetEvent, null, null);

                    KeyMessageEncryptionModel newKey = null;

                    if (autoResetEvent.WaitOne(3000) == false)
                    {
                        WindowManager.ShowMessagebox("С данными p и q слишком долгая генерация, выберите другие значения", false);
                        return;
                    }
                    else
                    {
                        newKey = delegateGenerateNewKey.EndInvoke(r1);
                    }

                    // KeyMessageEncryptionModel newKey = KeyMessageEncryptionModel.GenerateNewKey(ValueQ, ValueP);
                    Account.CurrentKeyMessagesEncryption = newKey;
                    AccountModel.SaveAccount(Account);

                    WindowManager.ShowMessagebox("Ключ создан и назначен текущим", false);
                    IsNewKey = !IsNewKey;
                    ValueQ = 0;
                    ValueP = 0;
                }


                //if (CheckLoginField())
                //{
                //    // Выполняем сохранение
                //    Account.CurrentPassword = this.Password;
                //    Account.Login = this.Login;

                //    if (DataManager.GetInstance().CreateNewAccount(Account))
                //    {
                //        CloseWindow(this);
                //        WindowManager.ShowMessagebox("ConfirmCreateAcc");
                //    }
                //}
            }
            // Если это работа с текущим ключем
            else
            {
                IsNewKey = !IsNewKey;
                //if (Login != Account.CurrentPassword)
                //{
                //    WindowManager.ShowMessagebox("PasswordsNotEquals");
                //}

                //PasswordModel password = new PasswordModel
                //{
                //    TimeCreate = DateTime.Now,
                //    Password = Password
                //};

                //if (DataManager.GetInstance().PasswordChange(Account, password))
                //{
                //    CloseWindow(this);
                //    WindowManager.ShowMessagebox("ConfirmPassChange");
                //}
            }
        }
        #endregion
    }
}
